LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_LDLIBS := -llog
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog

LOCAL_CERTIFICATE := shared

LOCAL_MODULE    := ndk_cpuaffinity
LOCAL_SRC_FILES := CPUAffinity.c

include $(BUILD_SHARED_LIBRARY)
