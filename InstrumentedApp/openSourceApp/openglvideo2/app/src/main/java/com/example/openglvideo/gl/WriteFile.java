package com.example.openglvideo.gl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import android.os.Environment;
import android.util.Log;

import com.opencsv.CSVWriter;

public class WriteFile {

	CSVWriter csvwriter;
	String csvpath;
	
	WriteFile()
	{
		File filedirectory=new File(Environment.getExternalStorageDirectory().getPath()+"/seedsgarden");
		filedirectory.mkdirs();
		
		Log.d("WriteFile", "creating the directory");
		
		Random r = new Random();
		int i1 = r.nextInt(9999999 - 1000000 + 1) + 1000000;
		
		try {
			File heapFile = new File(filedirectory, Integer.toString(i1)+".csv");
			Log.d("WriteFile", "creating the file1");
//			heapFile.createNewFile();
			Log.d("WriteFile", "creating the file2");
			FileWriter filewriter=new FileWriter(heapFile, true);
			Log.d("WriteFile", "creating the file writer");
			csvwriter=new CSVWriter(filewriter);
			Log.d("WriteFile", "have create the writer");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void write(String[] content)
	{
		csvwriter.writeNext(content);
	}
	
	void close()
	{
		try {
			csvwriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
