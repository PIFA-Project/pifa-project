package com.example.openglvideo;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class CPUAffinityService extends Service {

	public static native int setCPUAffinity(int mypid, int cpunum);
	
	static {
	    System.loadLibrary("ndk_cpuaffinity");
	}
	
	@SuppressLint("NewApi")
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		int cpu=intent.getIntExtra("CPU", -1);
		int pid=setCPUAffinity(android.system.Os.getpid(),cpu);
		Log.d("CPUAffinity", "cpu: "+cpu);
		Log.d("CPUAffinity", "pid: "+pid);
		return START_NOT_STICKY;
		}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}
