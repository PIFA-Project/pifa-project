package com.vickie.videojohnoliver.gl;

import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.util.Log;

import javax.microedition.khronos.egl.*;

/**
 * Renderer which initializes OpenGL 2.0 context on a passed surface and starts a rendering thread
 *
 * This class has to be subclassed to be used properly
 */
public abstract class TextureSurfaceRenderer implements Runnable
{
    private static final int EGL_OPENGL_ES2_BIT = 4;
    private static final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
    private static final String LOG_TAG = "SurfaceTest.GL";
    protected final SurfaceTexture texture;
    private EGL10 egl;
    private EGLDisplay eglDisplay;
    private EGLContext eglContext;
    private EGLSurface eglSurface;
    private Object lock;

    protected int width;
    protected int height;
    private boolean running;
    
    private Context context;

    /**
     * @param texture Surface texture on which to render. This has to be called AFTER the texture became available
     * @param width Width of the passed surface
     * @param height Height of the passed surface
     */
    public TextureSurfaceRenderer(SurfaceTexture texture, int width, int height, Context context)
    {
        this.texture = texture;
        this.width = width;
        this.height = height;
        this.running = true;
        this.context=context;
        lock=new Object();
        
        Thread thrd = new Thread(this);
        thrd.start();
    }

    @Override
    public void run()
    {
    	float averFrameTime=0;
    	int framenum=0;
        initGL();
        initGLComponents();

        while (running)
        {
            long loopStart = System.currentTimeMillis();
            long loopEnd;
            pingFps();

            if (draw())
            {
                egl.eglSwapBuffers(eglDisplay, eglSurface);
            }

            loopEnd=System.currentTimeMillis();
            long waitDelta = 34 - (loopEnd - loopStart);    // Targeting 60 fps, no need for faster
//            Log.d("TextureSurfaceRenderer", "ms per frame"+(System.currentTimeMillis()-loopStart));
            if (waitDelta > 0)
            {
                try
                {
                    Thread.sleep(waitDelta);
                }
                catch (InterruptedException e)
                {
                    continue;
                }
            }
            
            if(framenum<30)
            {
            	framenum++;
            	averFrameTime+=(float)(loopEnd-loopStart);
            }
            else{
            	framenum++;
            	averFrameTime+=(float)(loopEnd-loopStart);
            	averFrameTime=averFrameTime/framenum;
            	
            	Log.d("TextureSurface", "ms for each frame: "+averFrameTime);
            	Intent intent1=new Intent();
            	intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
        		intent1.putExtra("AppName","com.vickie.videojohnoliver");
        		intent1.putExtra("Performance", averFrameTime);
        		framenum=0;
        		averFrameTime=0;
//        		context.startService(intent1);
            }
        }

        deinitGLComponents();
        deinitGL();
    }

    /**
     * Main draw function, subclass this and add custom drawing code here. The rendering thread will attempt to limit
     * FPS to 60 to keep CPU usage low.
     */
    protected abstract boolean draw();

    /**
     * OpenGL component initialization funcion. This is called after OpenGL context has been initialized on the rendering thread.
     * Subclass this and initialize shaders / textures / other GL related components here.
     */
    protected abstract void initGLComponents();
    protected abstract void deinitGLComponents();

    private long lastFpsOutput = 0;
    private int frames;
    private void pingFps()
    {
        if (lastFpsOutput == 0)
            lastFpsOutput = System.currentTimeMillis();

        frames ++;

        if (System.currentTimeMillis() - lastFpsOutput > 1000)
        {
            Log.d(LOG_TAG, "FPS: " + frames);
            lastFpsOutput = System.currentTimeMillis();
            frames = 0;
        }
    }


    /**
     * Call when activity pauses. This stops the rendering thread and deinitializes OpenGL.
     */
    public void onPause()
    {
        running = false;
    }


    protected void initGL()
    {
    	Log.d("TextureSurfaceRenderer", "start initGL");
        egl = (EGL10) EGLContext.getEGL();
        eglDisplay = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);

        int[] version = new int[2];
        egl.eglInitialize(eglDisplay, version);

        Log.d("TextureSurfaceRenderer", "initGL 1");
        EGLConfig eglConfig = chooseEglConfig();
        Log.d("TextureSurfaceRenderer", "initGL 2");
        eglContext = createContext(egl, eglDisplay, eglConfig);

        Log.d("TextureSurfaceRenderer", "initGL 3");
        
        eglSurface = egl.eglCreateWindowSurface(eglDisplay, eglConfig, texture, null);

        if (eglSurface == null || eglSurface == EGL10.EGL_NO_SURFACE)
        {
            throw new RuntimeException("GL Error: " + GLUtils.getEGLErrorString(egl.eglGetError()));
        }

        Log.d("TextureSurfaceRenderer", "initGL 4");
        
        if (!egl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext))
        {
            throw new RuntimeException("GL Make current error: " + GLUtils.getEGLErrorString(egl.eglGetError()));
        }
        Log.d("TextureSurfaceRenderer", "end initGL");
    }

    private void deinitGL()
    {
        egl.eglMakeCurrent(eglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
        egl.eglDestroySurface(eglDisplay, eglSurface);
        egl.eglDestroyContext(eglDisplay, eglContext);
        egl.eglTerminate(eglDisplay);
        Log.d(LOG_TAG, "OpenGL deinit OK.");
    }

    private EGLContext createContext(EGL10 egl, EGLDisplay eglDisplay, EGLConfig eglConfig)
    {
        int[] attribList = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE };
        Log.d("TextureSurfaceRenderer", "createContext");
        return egl.eglCreateContext(eglDisplay, eglConfig, EGL10.EGL_NO_CONTEXT, attribList);
    }

    private EGLConfig chooseEglConfig()
    {
        int[] configsCount = new int[1];
        EGLConfig[] configs = new EGLConfig[1];
        int[] configSpec = getConfig();

        if (!egl.eglChooseConfig(eglDisplay, configSpec, configs, 1, configsCount))
        {
            throw new IllegalArgumentException("Failed to choose config: " + GLUtils.getEGLErrorString(egl.eglGetError()));
        }
        else if (configsCount[0] > 0)
        {
            return configs[0];
        }

        return null;
    }

    private int[] getConfig()
    {
        return new int[] {
                EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                EGL10.EGL_RED_SIZE, 8,
                EGL10.EGL_GREEN_SIZE, 8,
                EGL10.EGL_BLUE_SIZE, 8,
                EGL10.EGL_ALPHA_SIZE, 8,
                EGL10.EGL_DEPTH_SIZE, 0,
                EGL10.EGL_STENCIL_SIZE, 0,
                EGL10.EGL_NONE
        };
    }

    @Override
    protected void finalize() throws Throwable
    {
        super.finalize();
        running = false;
    }
}