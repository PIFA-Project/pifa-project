package com.vickie.videojohnoliver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.WindowManager;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.vickie.videojohnoliver.gl.VideoTextureRenderer;

public class SurfaceActivity extends Activity implements TextureView.SurfaceTextureListener
{
    private static final String LOG_TAG = "SurfaceTest";

    private TextureView surface;
    private MediaPlayer player;
    private VideoTextureRenderer renderer;

    private int surfaceWidth;
    private int surfaceHeight;
    
    private String path;
    long startTime, endTime;
    long FendTime;
    
    public static Context context;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.surface);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
/*        LinearLayout lnr=(LinearLayout)findViewById(R.id.abc);
        Button b1 = new Button(this);
        b1.setText("Btn");
        lnr.addView(b1);*/
        
        Intent intent=getIntent();
        path=intent.getStringExtra("Path");
        Log.d("SurfaceActivity", "onCreate");
        surface = (TextureView) findViewById(R.id.surface);
        surface.setSurfaceTextureListener(this);
        
        context=getApplicationContext();
        
        Timer timer=new Timer(true);
        TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				/*try {
					Process su = Runtime.getRuntime().exec("su");
					DataOutputStream outputstream=new DataOutputStream(su.getOutputStream());
					outputstream.writeBytes("am force-stop com.vickie.videojohnoliver\n");
					outputstream.flush();		
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
				finish();
			}
			};
			
//			timer.schedule(timerTask, 60000, 1000);
        
			startTime=getIntent().getLongExtra("startTime", -1);
			endTime=SystemClock.uptimeMillis();
			
			Intent intent1=new Intent();
			intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
			intent1.putExtra("AppName","com.vickie.videojohnoliver");
			intent1.putExtra("PhaseNumber", 0);
			long exeTime=endTime-startTime;
			intent1.putExtra("Performance", (float) exeTime);
//			startService(intent1);

	
			FendTime=System.currentTimeMillis();
			Log.d("SurfaceActivity", Long.toString(FendTime-FileExplore.FstartTime));
			Intent intent2=new Intent();
			intent2.setClassName("com.vickie.testforappservice", "com.vickie.testforappservice.testForAppService");
    		intent2.putExtra("AppName","com.vickie.videojohnoliver");
    		intent2.putExtra("Performance", (float)(FendTime-FileExplore.FstartTime));
  //  		startService(intent2);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (surface.isAvailable())
            startPlaying();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (player != null)
            player.release();
        if (renderer != null)
            renderer.onPause();
    }

    @SuppressLint("SdCardPath")
	private void startPlaying()
    {
        renderer = new VideoTextureRenderer(this, surface.getSurfaceTexture(), surfaceWidth, surfaceHeight);
        player = new MediaPlayer();

        try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        try
        {
//            AssetFileDescriptor afd = getAssets().openFd("JohnOliver_360p_mp4.mp4");
//            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        	player.setDataSource(getBaseContext(), Uri.parse(path));
            Log.d("SurfaceActivity", "Before openFd");
            player.setSurface(new Surface(renderer.getVideoTexture()));
            Log.d("SurfaceActivity", "After openFd");
            player.setLooping(true);
            player.prepare();
            renderer.setVideoSize(player.getVideoWidth(), player.getVideoHeight());
            player.start();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Could not open input video!");
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
    {
        surfaceWidth = width;
        surfaceHeight = height;
        startPlaying();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
    {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface)
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }
    public void onDestroy()
    {
    	Log.d("SurfaceActivity", "onDestroy");
    	
		Intent intent2=new Intent();
		intent2.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.StartScheduler");
		intent2.putExtra("AppName","com.vickie.videojohnoliver");
//		startService(intent2);
		
    	super.onDestroy();
    }
}
