package com.vickie.videojohnoliver;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

public class FileExplore extends Activity {

	// Stores names of traversed directories
	ArrayList<String> str = new ArrayList<String>();

	// Check if the first level of the directory structure is the one showing
	private Boolean firstLvl = true;

	private static final String TAG = "F_PATH";

	private Item[] fileList;
	private File path = new File(Environment.getExternalStorageDirectory() + "");
	private String chosenFile;
	private static final int DIALOG_LOAD_FILE = 1000;

	private Button load, play;
	private TextView showPath;
	private String pathValue;
	private float[] msFrame;
	String output;
	Timer timer;
	TimerTask timerTask;
//	static public TextureView surface;
	
	public static long FstartTime;
	public long endTime;
	
	ListAdapter adapter;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		load=(Button)findViewById(R.id.button2);
		play=(Button)findViewById(R.id.button1);
//		surface=(TextureView)findViewById(R.id.surface);
		
		showPath=(TextView)findViewById(R.id.textView1);
		CPUAffinityService.setCPUAffinity(android.system.Os.getpid(),0);
//		showPath.setText("None");
		
/*		Intent intent1=new Intent();
		intent1.setClassName("com.example.phaseidexample", "com.example.phaseidexample.PhaseService");
		intent1.putExtra("Instruction","start");
		intent1.putExtra("PID", android.os.Process.myPid());
		intent1.putExtra("CPU", 0);
		startService(intent1);*/
		msFrame=new float[30];
				
		timer=new Timer(true);
        timerTask = new TimerTask(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				for(int i=0; i<msFrame.length; i++)
					msFrame[i]=0;
				
				int averFrameTime=0;
				Log.d("FileExplore", "running");
				
				try {
					Process process=Runtime.getRuntime().exec("su");
//					BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
					DataOutputStream outputstream=new DataOutputStream(process.getOutputStream());
					
					outputstream.writeBytes("dumpsys gfxinfo com.vickie.videojohnoliver | grep -A 20 \'Prepare\' > /sdcard/GfxInfo/gfxinfo.txt\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				Log.d("FileExplore", "Before gfxinfo: "+Arrays.toString(msFrame));
				
				if(ObtainFrameInfo(msFrame)==true)
				{
//					Log.d("FileExplore", "After gfxinfo: "+Arrays.toString(msFrame));
					
					int count=0;
					for(int i=0; msFrame[i]!=0; i++)
					{
						averFrameTime+=msFrame[i];
						count++;
					}
					averFrameTime/=count;
					
					Intent intent1=new Intent();
	            	intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
	        		intent1.putExtra("AppName","com.vickie.videojohnoliver");
	        		intent1.putExtra("Performance", averFrameTime);
//	        		startService(intent1);
	        		
	        		Log.d("FileExplore", "Average Frame Time: "+averFrameTime);
				}
				else
					Log.d("FileExplore", "No GfxInfo Captured");
			
			}
        };
        
 //       timer.schedule(timerTask, 1000, 1000);
	}

	public void LoadPath(View view)
	{
		FstartTime=System.currentTimeMillis();
		
		loadFileList();

		showDialog(DIALOG_LOAD_FILE);
		Log.d(TAG, path.getAbsolutePath());
		endTime=System.currentTimeMillis();
		
		Log.d("FileExplore", Long.toString(endTime-FstartTime));
		
		Intent intent1=new Intent();
//		intent1.setClassName("com.vickie.testforappservice", "com.vickie.testforappservice.testForAppService");
		intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
		intent1.putExtra("AppName", "com.vickie.videojohnoliver");
		intent1.putExtra("Performance",(float)(endTime-FstartTime));
//		startService(intent1);
		InstrumentedWFile.Response(endTime-FstartTime);
	}
	
	public void Play(View view)
	{
		long startTime=SystemClock.uptimeMillis();
		FstartTime=System.currentTimeMillis();
		Intent intent = new Intent(this, SurfaceActivity.class);
		intent.putExtra("Path", pathValue);
		intent.putExtra("startTime", startTime);
		Log.d("FileExplore", "Play");
		startActivity(intent);
	}
	
	private void loadFileList() {
		try {
			path.mkdirs();
		} catch (SecurityException e) {
			Log.e(TAG, "unable to write on the sd card ");
		}

		// Checks whether path exists
		if (path.exists()) {
			FilenameFilter filter = new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					File sel = new File(dir, filename);
					// Filters based on whether the file is hidden or not
					return (sel.isFile() || sel.isDirectory())
							&& !sel.isHidden();

				}
			};

			String[] fList = path.list(filter);
			fileList = new Item[fList.length];
			for (int i = 0; i < fList.length; i++) {
				fileList[i] = new Item(fList[i], R.drawable.file_icon);

				// Convert into file path
				File sel = new File(path, fList[i]);

				// Set drawables
				if (sel.isDirectory()) {
					fileList[i].icon = R.drawable.directory_icon;
//					Log.d("DIRECTORY", fileList[i].file);
				} else {
//					Log.d("FILE", fileList[i].file);
				}
			}

			if (!firstLvl) {
				Item temp[] = new Item[fileList.length + 1];
				for (int i = 0; i < fileList.length; i++) {
					temp[i + 1] = fileList[i];
				}
				temp[0] = new Item("Up", R.drawable.directory_up);
				fileList = temp;
			}
		} else {
			Log.e(TAG, "path does not exist");
		}

		adapter = new ArrayAdapter<Item>(this,
				android.R.layout.select_dialog_item, android.R.id.text1,
				fileList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// creates view
				View view = super.getView(position, convertView, parent);
				TextView textView = (TextView) view
						.findViewById(android.R.id.text1);

				// put the image on the text view
				textView.setCompoundDrawablesWithIntrinsicBounds(
						fileList[position].icon, 0, 0, 0);

				// add margin between image and text (support various screen
				// densities)
				int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
				textView.setCompoundDrawablePadding(dp5);

				return view;
			}
		};

	}

	private class Item {
		public String file;
		public int icon;

		public Item(String file, Integer icon) {
			this.file = file;
			this.icon = icon;
		}

		@Override
		public String toString() {
			return file;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		AlertDialog.Builder builder = new Builder(this);

		if (fileList == null) {
			Log.e(TAG, "No files loaded");
			dialog = builder.create();
			return dialog;
		}

		switch (id) {
		case DIALOG_LOAD_FILE:
			builder.setTitle("Choose your file");
			builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					chosenFile = fileList[which].file;
					File sel = new File(path + "/" + chosenFile);
					if (sel.isDirectory()) {
						firstLvl = false;

						// Adds chosen directory to list
						str.add(chosenFile);
						fileList = null;
						path = new File(sel + "");

						loadFileList();

						removeDialog(DIALOG_LOAD_FILE);
						showDialog(DIALOG_LOAD_FILE);
						Log.d("Directory", path.getAbsolutePath());

					}

					// Checks if 'up' was clicked
					else if (chosenFile.equalsIgnoreCase("up") && !sel.exists()) {

						// present directory removed from list
						String s = str.remove(str.size() - 1);

						// path modified to exclude present directory
						path = new File(path.toString().substring(0,
								path.toString().lastIndexOf(s)));
						fileList = null;

						// if there are no more directories in the list, then
						// its the first level
						if (str.isEmpty()) {
							firstLvl = true;
						}
						loadFileList();

						removeDialog(DIALOG_LOAD_FILE);
						showDialog(DIALOG_LOAD_FILE);
						Log.d("Up", path.getAbsolutePath());

					}
					// File picked
					else {
						// Perform action with file picked
						pathValue=sel.getPath();
						showPath.setText(pathValue);
						Log.d("File", pathValue);
					}

				}
			});
			break;
		}
		dialog = builder.show();
		
		return dialog;
	}
	
	public void onDestroy()
	{
		super.onDestroy();
/*		Intent intent2=new Intent();
		intent2.setClassName("com.example.phaseidexample", "com.example.phaseidexample.PhaseService");
		intent2.putExtra("Instruction","stop");
		startService(intent2);*/
	}

	public boolean ObtainFrameInfo(float[] msFrame)
	{
		String string;
		String[] framedata;
		int count=0;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("/sdcard/GfxInfo/gfxinfo.txt"));
			
			string=br.readLine();
			
			while(string==null||!string.contains("Prepare"))
				string=br.readLine();
			
			string=br.readLine();
			while(string==null||!string.contains("Prepare"))
			{
				if(string==null)
					return false;
				string=br.readLine();
			}
			string=br.readLine();
			Log.d("FileExplore", string);
			if(string.equals(""))
				return false;
			while(string!=null&&!string.equals(""))
			{
				framedata=string.split("\t");
				
				for(String a:framedata)
				{
					if(!a.equals(""))
						msFrame[count]+=Float.valueOf(a);
				}
				count++;
				string=br.readLine();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
}
