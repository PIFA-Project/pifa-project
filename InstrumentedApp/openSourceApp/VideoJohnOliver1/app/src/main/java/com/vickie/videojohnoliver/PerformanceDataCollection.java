package com.vickie.videojohnoliver;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;

public class PerformanceDataCollection {
	static long msFrame[];
	static double average;
	static int count;
	static Timer timer;
	static TimerTask timerTask;
	
	public static void initPerformanceDataCollection()
	{
		msFrame = new long[1000];
		count=0;
		average=0;
	}
	
	public static void collectData(long performance)
	{
		msFrame[count++]=performance;
	}
	
	public static void startTimer()
	{
		timer=new Timer(true);
        timerTask = new TimerTask(){

			@SuppressLint("LongLogTag")
			@Override
			public void run() {
				for(int i=0; i<count; i++)
				{
					average+=(double)msFrame[i];
					msFrame[i]=0;
				}
				average/=count;
				
				Intent intent1=new Intent();
				intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
				intent1.putExtra("AppName","com.king.candycrushsaga");
//				intent1.putExtra("PhaseNumber", 0);
				intent1.putExtra("Performance",(float)average);
//				SurfaceActivity.context.startService(intent1);
				
				Log.d("PerformanceDataCollection", "timer running");
				
				average=0;
				count=0;
			}
        };
        
        for(int i=0; i<count; i++)
		{
			average+=(double)msFrame[i];
			msFrame[i]=0;
		}
        count=0;
		average=0;
        
        timer.schedule(timerTask, 1000, 1000);
	}
	
	public static void stopTimer()
	{
		timer.cancel();
	}
}
