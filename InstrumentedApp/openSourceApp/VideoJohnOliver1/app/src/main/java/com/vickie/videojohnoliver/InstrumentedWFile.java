package com.vickie.videojohnoliver;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by xia on 4/7/16.
 */
public class InstrumentedWFile {
    static public void Response(long time)
    {
        try {
            File file = new File(Environment.getExternalStorageDirectory(),"response.txt");
//            Log.d("MainMenu1",file.getPath());
            FileOutputStream fileout=new FileOutputStream(file);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write(Long.toString(time));
            outputWriter.close();
//            Log.d("response", string+" "+Long.toString(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        WriteExternalState.write("SurfaceActivity");
    }

    static public void State(String string) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "state.txt");
//            Log.d("MainMenu1",file.getPath());
            FileOutputStream fileout = new FileOutputStream(file);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(string);
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
