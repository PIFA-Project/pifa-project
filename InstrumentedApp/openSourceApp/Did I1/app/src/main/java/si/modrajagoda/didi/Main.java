package si.modrajagoda.didi;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class Main extends FragmentActivity implements ActionBar.TabListener {

	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	private static final String TAB_TAG_HABITS = "habits";
	private static final String TAB_TAG_PROGRESS = "progress";
	
	static public long startTime;
	static public long endTime;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		Log.d("Main", "onCreate");
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		Log.d("Main", "1");

		// For each of the sections in the app, add a tab to the action bar.
		actionBar.addTab(actionBar.newTab().setText(R.string.title_habits)
				.setTag(TAB_TAG_HABITS).setTabListener(this));
		Log.d("Main", "2");
		actionBar.addTab(actionBar.newTab().setText(R.string.title_progress)
				.setTag(TAB_TAG_PROGRESS).setTabListener(this));
		Log.d("Main", "3");
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
//		Log.d("Main", "onRestoreInstanceState");
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
//		Log.d("Main", "onSaveInstanceState");
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
		endTime=SystemClock.uptimeMillis();
//		Log.d("Main", "onSaveInstanceState "+endTime);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		Log.d("Main", "onCreateOptionsMen: "+SystemClock.uptimeMillis());
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		startTime=SystemClock.uptimeMillis();
//		Log.d("Main", "onOptionsItemSelected: "+startTime);
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intent = new Intent(this, EditHabits.class);
			startActivity(intent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
//		Log.d("Main", "onTabSelected "+Long.toString(SystemClock.uptimeMillis()));
		// When the given tab is selected, show the tab contents in the
		// container
		if (tab.getTag() == TAB_TAG_HABITS) {
			Fragment fragment = new FragmentHabits();
			Bundle args = new Bundle();
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
		}
		if (tab.getTag() == TAB_TAG_PROGRESS) {
			Fragment fragment = new FragmentProgress();
			Bundle args = new Bundle();
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
		}
	
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}
	
	public void onDestroy() {
		Intent intent2=new Intent();
		intent2.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.StartScheduler");
		intent2.putExtra("AppName","si.modrajagoda.didi");
//		startService(intent2);
		super.onDestroy();
		Log.d("Main", "onDestroy");
	}

	@Override
	public void onResume()
	{
		super.onResume();
		try {
			File file = new File(Environment.getExternalStorageDirectory(),"state.txt");
//            Log.d("MainMenu1",file.getPath());
			FileOutputStream fileout=new FileOutputStream(file);
			OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
			outputWriter.write("Main");
			outputWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
