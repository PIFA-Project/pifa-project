package si.modrajagoda.didi;

import java.io.DataOutputStream;
import java.io.IOException;

import android.os.SystemClock;

public class WriteFile {

	static void writeCurrentTime(long exetime, String path)
	{	
		try {
			Process suCPU=Runtime.getRuntime().exec("su");
			DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());
			outputstream.writeBytes("echo "+Long.toString(exetime)+" >> "+path+"\n");
			outputstream.flush();
			
			outputstream.writeBytes("exit\n");
			outputstream.flush();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
