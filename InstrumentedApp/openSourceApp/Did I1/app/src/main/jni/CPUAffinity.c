#include <jni.h>
#include <string.h>

#define _GNU_SOURCE

#include <android/log.h>
#include <sched.h>
#include <sys/syscall.h>
#include <pthread.h>

#define DEBUG_TAG "NDK_MainActivity"

jint Java_si_modrajagoda_didi_CPUAffinityService_setCPUAffinity(JNIEnv *env, jobject javaThis, jint mypid, jint cpunum)
{

	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(cpunum, &mask);
	if( sched_setaffinity(mypid, sizeof(mask), &mask ) == -1 )
	{
      		return errno;
	}

	return mypid;
}
