package org.jtb.alogcat;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.WindowManager;

import org.jtb.alogcat.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class PrefsActivity extends PreferenceActivity implements
		OnSharedPreferenceChangeListener {
	private ListPreference mLevelPreference;
	private ListPreference mFormatPreference;
	private ListPreference mBufferPreference;
	private ListPreference mTextsizePreference;
	private ListPreference mBackgroundColorPreference;
	
	private Prefs mPrefs;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.prefs);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		mPrefs = new Prefs(this);
		
		mLevelPreference = (ListPreference) getPreferenceScreen()
		.findPreference(Prefs.LEVEL_KEY);
		mFormatPreference = (ListPreference) getPreferenceScreen()
		.findPreference(Prefs.FORMAT_KEY);
		mBufferPreference = (ListPreference) getPreferenceScreen()
		.findPreference(Prefs.BUFFER_KEY);
		mTextsizePreference = (ListPreference) getPreferenceScreen()
		.findPreference(Prefs.TEXTSIZE_KEY);
		mBackgroundColorPreference = (ListPreference) getPreferenceScreen()
		.findPreference(Prefs.BACKGROUND_COLOR_KEY);
		
		setResult(Activity.RESULT_OK);
		
		android.content.Intent intent=getIntent();
		long startTime=intent.getLongExtra("StartTime", 0);
		long endTime=SystemClock.uptimeMillis();
		long exeTime=endTime-startTime;
		Log.d("LogActivity", "MenuPrefs: " + (endTime - startTime));
		
		Intent intent1=new Intent();
		intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
		intent1.putExtra("AppName","org.jtb.alogcat");
		intent1.putExtra("Performance", (float) exeTime);
//		startService(intent1);
	}

	private void setLevelTitle() {
		mLevelPreference.setTitle("Level? (" + mPrefs.getLevel().getTitle(this) + ")");
	}

	private void setFormatTitle() {
		mFormatPreference.setTitle("Format? (" + mPrefs.getFormat().getTitle(this) + ")");
	}

	private void setBufferTitle() {
		mBufferPreference.setTitle("Buffer? (" + mPrefs.getBuffer().getTitle(this) + ")");
	}

	private void setTextsizeTitle() {
		mTextsizePreference.setTitle("Text Size? (" + mPrefs.getTextsize().getTitle(this) + ")");
	}
	
	private void setBackgroundColorTitle() {
		mBackgroundColorPreference.setTitle("Background Color? (" + mPrefs.getBackgroundColor().getTitle(this) + ")");
	}

	@Override
	protected void onResume() {
		super.onResume();

		setLevelTitle();
		setFormatTitle();
		setBufferTitle();
		setTextsizeTitle();
		setBackgroundColorTitle();
		
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);

		try {
			File file = new File(Environment.getExternalStorageDirectory(),"state.txt");
//            Log.d("MainMenu1",file.getPath());
			FileOutputStream fileout=new FileOutputStream(file);
			OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
			outputWriter.write("SurfaceActivity");
			outputWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(Prefs.LEVEL_KEY)) {
			setLevelTitle();
		} else if (key.equals(Prefs.FORMAT_KEY)) {
			setFormatTitle();
		} else if (key.equals(Prefs.BUFFER_KEY)) {
			setBufferTitle();
		} else if (key.equals(Prefs.TEXTSIZE_KEY)) {
			setTextsizeTitle();
		} else if (key.equals(Prefs.BACKGROUND_COLOR_KEY)) {
			setBackgroundColorTitle();
		}
	}
}
