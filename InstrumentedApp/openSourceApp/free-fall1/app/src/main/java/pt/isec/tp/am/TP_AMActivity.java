package pt.isec.tp.am;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

public class TP_AMActivity extends Activity {
    /** Called when the activity is first created. */
	
	Timer timer=new Timer(true);
	
	static long startTime;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
 //       Timer timer=new Timer(true);
        TimerTask timerTask = new TimerTask() {

			@SuppressLint("NewApi")
			@Override
			public void run() {
				// TODO Auto-generated method stub
				/*try {
					Process su = Runtime.getRuntime().exec("su");
					DataOutputStream outputstream=new DataOutputStream(su.getOutputStream());
					outputstream.writeBytes("am force-stop pt.isec.tp.am\n");
					outputstream.flush();		
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
				Intent intent = new Intent("finish_activity");
				sendBroadcast(intent);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finish();
			}
			};
			
//			timer.schedule(timerTask, 40000, 1000);
			
/*			Intent intent=new Intent();
			intent.setClassName("com.example.phaseidexample", "com.example.phaseidexample.PhaseService");
			intent.putExtra("Instruction","start");
			intent.putExtra("PID", android.os.Process.myPid());
			intent.putExtra("CPU", 0);
			startService(intent);*/
        
        Log.d("TP_AMActivity", "onCreate");
    }
    
    public void onJogarPressionado(View v)
    {
    	startActivity(new Intent(TP_AMActivity.this, GameActivity.class));
    }

    public void onCliclAbout(View V)
    {
    	Intent intent = new Intent(this,About.class);
    	startActivity(intent);
    	
    }
    
    public void OnClickExit(View v)
    {
    	
    	new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(getResources().getString(R.string.quitDialogTitle))
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {

            finish();	               
            }

        }
        ).setNegativeButton(android.R.string.no, null).show();
    }

    public void onClickScore(View v)
    {
    	Intent intent = new Intent(this, Score.class);
    	startActivity(intent);
    }
    
    public void OnClickSettings(View v){
    	
    	Intent intent = new Intent(this, MyPreferencesActivity.class);
    	startActivity(intent);
    }
    
    public void onClickHelp(View v){
    	
    	startTime=SystemClock.uptimeMillis();
    	Intent intent = new Intent(this, Help.class);
    	startActivity(intent);
    }
    
    public void onDestroy()
    {
    	Log.d("TP_AMActivity", "onDestroy");
    	
    	timer.cancel();
    	
    	/*try {
		Process su = Runtime.getRuntime().exec("su");
		DataOutputStream outputstream=new DataOutputStream(su.getOutputStream());
		outputstream.writeBytes("am startservice --es AppName pt.isec.tp.am com.vickie.anroidem2/.StartScheduler\n");
		outputstream.flush();		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
    	
    	Intent intent2=new Intent();
		intent2.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.StartScheduler");
		intent2.putExtra("AppName","pt.isec.tp.am");
//		startService(intent2);
    	
/*    	Intent intent=new Intent();
		intent.setClassName("com.example.phaseidexample", "com.example.phaseidexample.PhaseService");
		intent.putExtra("Instruction","stop");
		startService(intent);*/
    	super.onDestroy();
    }
}