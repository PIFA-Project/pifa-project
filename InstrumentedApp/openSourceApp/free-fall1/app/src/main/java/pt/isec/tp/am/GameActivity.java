package pt.isec.tp.am;

import java.util.Timer;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

public class GameActivity extends Activity {
	
	Timer m_timer;
	Surface gameView;
	Model model;
	Thread tmodel;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        gameView = new Surface(this.getBaseContext());
        this.setContentView(gameView);
        
        model = new Model(this, gameView);
        tmodel = new Thread(model);
        tmodel.start();
        
        BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
//                	onPause();
                    finish();
                    // DO WHATEVER YOU WANT.
                }
            }
        };
        registerReceiver(broadcast_reciever, new IntentFilter("finish_activity"));
   }
    
    public void onResume() {
    	super.onResume();
    	//tmodel = new Thread(model);
    }
    
    
    public void onPause() {
    	super.onPause(); 
    	Log.d("GameActivity", "onPause");
    	model.terminate();
    	try {
    		tmodel.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	if ( this.isFinishing() )
    	{
        	//Intent intent = new Intent(this, Score.class);
        	//startActivity(intent);
    	}
    	Surface.timer.cancel();
    }
    
    public void onDestroy() {
    	Log.d("GameActivity", "On Destroy");

    	Surface.timer.cancel();
    	super.onDestroy();
    }
    
}