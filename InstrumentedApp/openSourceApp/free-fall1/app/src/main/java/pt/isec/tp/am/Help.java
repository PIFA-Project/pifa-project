package pt.isec.tp.am;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.TabHost;

public class Help extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		
		final TabHost tabhost = (TabHost) findViewById(android.R.id.tabhost);
		tabhost.setup();
		tabhost.addTab(tabhost.newTabSpec("tab_help").setIndicator(getResources().getString(R.string.Help, (Object[])null)).setContent(R.id.tab1));
		tabhost.addTab(tabhost.newTabSpec("tab_help_bonus").setIndicator(getResources().getString(R.string.Bonus, (Object[])null)).setContent(R.id.tab2));
		tabhost.setCurrentTab(0);
		
		Intent intent1=new Intent();
		intent1.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.Monitor");
		intent1.putExtra("AppName","pt.isec.tp.am");
		intent1.putExtra("PhaseNumber", 0);
		long endTime=SystemClock.uptimeMillis();
		long exeTime=endTime-TP_AMActivity.startTime;
		intent1.putExtra("Performance", (float) exeTime);
//		startService(intent1);
		InstrumentedWFile.Response(exeTime);
	}

	
	
	public void onBack(View v)
	{
		finish();
	}
	
}
