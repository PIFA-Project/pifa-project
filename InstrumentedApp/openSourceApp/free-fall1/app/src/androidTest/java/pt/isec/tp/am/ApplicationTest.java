package pt.isec.tp.am;

import android.app.Application;
import android.os.Environment;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.robotium.solo.Solo;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ApplicationTest extends ActivityInstrumentationTestCase2<Activity> {

    private static final String TARGET_PACKAGE_ID = "pt.isec.tp.am";
    private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "pt.isec.tp.am.TP_AMActivity";
    private static Class launcherActivityClass;
    static {
        try {
            launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @SuppressLint("NewApi")
    public ApplicationTest(){

        super((Class<Activity>)launcherActivityClass);
        // TODO Auto-generated constructor stub
    }

    private Solo solo;

    @Override
    protected void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testDisplayBlackBox() {


        solo.sleep(20000);
        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Help");
        solo.sleep(5000);
        solo.goBack();
        solo.sleep(4000);

        solo.clickOnButton("Play");
        solo.sleep(180000);


        solo.goBack();


        try {
            File file = new File(Environment.getExternalStorageDirectory(),"test.txt");
            Log.d("TestAPK",file.getPath());
            FileOutputStream fileout=new FileOutputStream(file);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write("no");
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
}

