LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := ndk_cpuaffinity
LOCAL_LDFLAGS := -Wl,--build-id
LOCAL_SRC_FILES := \
	/home/xia/AndroidStudioProjects/free-fall1/app/src/main/jni/CPUAffinity.c \
	/home/xia/AndroidStudioProjects/free-fall1/app/src/main/jni/Android.mk \

LOCAL_C_INCLUDES += /home/xia/AndroidStudioProjects/free-fall1/app/src/main/jni
LOCAL_C_INCLUDES += /home/xia/AndroidStudioProjects/free-fall1/app/src/debug/jni

include $(BUILD_SHARED_LIBRARY)
