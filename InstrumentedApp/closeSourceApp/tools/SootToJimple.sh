#!/bin/bash

apkname="com.imangi.templerun"
#sdkpath="/home/xia/Downloads/android-platforms-master/android-17"
sdkpath="/opt/android-sdk-linux/platforms/android-21"

rm -r ./sootOutput/*
java -Xmx4g -jar ./Soot/soot-trunk.jar soot.Main -p cg.spark verbose:true,on-fly-cg:true -w -allow-phantom-refs -force-android-jar $sdkpath -src-prec apk -f jimple -process-dir $apkname.apk -output-dir ./SootOutput_$apkname 
