#!/bin/bash

#sdkpath="/home/xia/Downloads/android-platforms-master/android-17"
sdkpath="/opt/android-sdk-linux/platforms/android-21"
apkname="com.tatkovlab.sdcardcleaner"
rm -r ./outAPK/*
unzip $apkname.apk -d ./outAPK
rm -r ./outAPK/META-INF ./outAPK/classes.dex

#java -Xmx2g -jar ./Soot/soot-trunk.jar soot.Main -force-android-jar  $sdkpath -w -allow-phantom-refs -src-prec jimple -output-format dex -process-dir ./sootOutput_$apkname

cp -i ./sootOutput/classes.dex ./outAPK
cd ./outAPK
zip -r ./$apkname.zip *
cd ..
mv ./outAPK/$apkname.zip ./outAPK/$apkname.apk
jarsigner -keystore ~/.android/debug.keystore -storepass android -keypass android ./outAPK/$apkname.apk androiddebugkey
adb install ./outAPK/$apkname.apk



