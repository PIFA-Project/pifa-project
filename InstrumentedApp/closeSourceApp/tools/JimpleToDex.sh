#!/bin/bash

sdkpath="/home/xia/Downloads/android-platforms-master/android-17"
apkname="com.rovio.angrybirds.apk"
rm -r ./outAPK/*
unzip $apkname -d ./outAPK

java -Xmx2g -jar ./Soot/soot-trunk.jar soot.Main -force-android-jar  $sdkpath -w -allow-phantom-refs -src-prec jimple -output-format dex -process-dir ./sootOutput

