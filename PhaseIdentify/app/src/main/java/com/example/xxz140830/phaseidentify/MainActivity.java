package com.example.xxz140830.phaseidentify;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    Button utilCollet, stop;
    AppEntity app;
    UtilCollector utilCollector;
    Timer timer;
    Timer stop_timer;
    TimerTask stop_timertask;
    WriteFile writefile;
    static File filepath;
    static Context context;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE=3;

    Timer timer_start_app;
    TimerTask timer_start_app_task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app=new AppEntity("com.king.candycrushsaga", "com.vickie.videojohnoliver.test", Apptype.GAMEAPP, 1958400, "CPUAffinityService", 33);
        CoreManagement.InitCoreManagement(4);
        context=getApplicationContext();
        filepath=Environment.getExternalStorageDirectory();
        SystemSet.setGovernor("performance");

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
            Log.d("MainActivity1", "Writing external storage granted");
            writefile = new WriteFile(app.AppName + "_phasedata.csv");
        }
        else {
            Log.d("MainActivity1", "Writing external storage denied");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        stop_timertask=new TimerTask() {
            @Override
            public void run() {
                timer.cancel();
                writefile.close();
                Log.d("OnCreate","Finish");
            }
        };

        timer_start_app=new Timer();
        timer_start_app_task=new TimerTask() {
            @Override
            public void run() {
                utilCollector= new UtilCollector(app, CoreManagement.core[0], writefile);
                SystemSet.runApp(app, CoreManagement.core[0], getApplicationContext());
//                context.startInstrumentation(new ComponentName(app.AppVideoTest, "android.test.InstrumentationTestRunner"), null, null);
                try {
                    Process suCPU=Runtime.getRuntime().exec("su");
                    DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    outputstream.writeBytes("am startservice --ei CPU 1 "+app.AppName+"/."+app.ServiceName+"\n");
//	    		Log.d("SystemSet", "am instrumentation "+app.AppVideoTest+"/android.test.InstrumentationTestRunner\n");
                    Log.d("SystemSet", "start CPU affinity service");
                    outputstream.flush();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                timer=new Timer();
                timer.schedule(utilCollector,0,1000);

                stop_timer=new Timer();
                stop_timer.schedule(stop_timertask,400000);
//                stop_timer.schedule(stop_timertask, 150000);
                Log.d("MainActivity1","after stop timer");
            }
        };

        timer_start_app.schedule(timer_start_app_task, 3000);

        utilCollet = (Button)findViewById(R.id.button);
        utilCollet.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    writefile = new WriteFile(app.AppName + "_phasedata.csv");
                } else {

                    Log.d("MainActivity1", "Permission to external storage failed");
                }
                return;
            }

        }
    }
}
