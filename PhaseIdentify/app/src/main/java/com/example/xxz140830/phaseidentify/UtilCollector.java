package com.example.xxz140830.phaseidentify;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Queue;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by xxz140830 on 1/31/2016.
 */
public class UtilCollector extends TimerTask{

    long cpuTotalBefore=0;
    long cpuPidBefore=0;
    AppEntity app;
    Core core;
    Queue<String> file;
    double cpuUse, gpuUse, memUse;
    int timeCounter=0;
    WriteFile writefile;

    public UtilCollector(AppEntity app, Core core, WriteFile writefile)
    {
        this.app=app;
        this.core=core;
        file=new LinkedBlockingQueue<String>();
        this.writefile=writefile;
    }

    public void run() {
        readPhase(cpuUse, gpuUse, memUse);
        timeCounter++;
        String string;


        Log.d("UtilCollector", " "+file.size());

        if(timeCounter==30)
        {
            timeCounter=0;

            while(!file.isEmpty())
            {
                string=file.remove();
                Log.d("UtilCollector","before writting file");
                writefile.write(string.split("#"));
            }
        }
    }

    public void readPhase(double cpuUsage, double gpuUsage, double memUsage)
    {
        gpuUse=gpuUsage();
        cpuUse=cpuUsage_App();
        memUse=memUsage_App();

//        Log.d("UtilCollector", gpuUse+" "+cpuUse+" "+memUse);

        Process process;
        String result = null;
        String state=null;
        try {
            process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream.readLine();
            while(result.equals(""))
                result=inputstream.readLine();
            inputstream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        Log.d("UtilCollector", "just after reading scaling cpu");

        File file1 = new File(Environment.getExternalStorageDirectory(),"state.txt");
        try {
            BufferedReader inputReader2 = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file1)));
            String inputString2;
            if ((inputString2 = inputReader2.readLine()) != null) {
                    state=inputString2;
//                Log.d("UtilCollector1", state);
                }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

/*        Log.d("UtilCollector", "before start log");
        try{
            Process logcat_reading=Runtime.getRuntime().exec("");
            BufferedReader instream=new BufferedReader(new InputStreamReader(logcat_reading.getInputStream()));

            result=null;
            int count=0;
            while(count<10)
            {
                count++;
                result=instream.readLine();
                Log.d("UtilCollector", "result "+count+" is : "+result);
                if(!result.contains("XIA")) {
                    Log.d("UtilCollector", "count0");
                    continue;
                }
                state=result.split(" ")[2];
                if(state.contains("android.app"))
                {
                    state=null;
                    Log.d("UtilCollector", "count1");
                    continue;
                }
                Log.d("UtilCollector", "count1");
                String[] strings=state.split(".");
                state=strings[strings.length-1];
            }

            instream.close();

            Runtime.getRuntime().exec("logcat -c");*/

//        }catch (IOException e){}

        file.add(cpuUse+"#"+gpuUse+"#"+memUse+"#"+result+"#"+core.CoreNumber+"#"+ SystemClock.uptimeMillis()+"#"+state);
    }

    public Double cpuUsage_App()
    {
        long cpuTotalAfter;
        long cpuPidAfter;

        double usage;

        Log.d("SystemSet", "cpuTotalBefore "+cpuTotalBefore);
        Log.d("SystemSet", "cpuPidBefore "+cpuPidBefore);

        if(cpuTotalBefore==0||cpuPidBefore==0)
        {
            cpuTotalBefore=getCPUTotal();
            cpuPidBefore=getCPUPid();
            return 0.00;
        }

        cpuTotalAfter=getCPUTotal();
        cpuPidAfter=getCPUPid();

        usage=(double)(cpuPidAfter-cpuPidBefore)/(cpuTotalAfter-cpuTotalBefore);
        cpuTotalBefore=cpuTotalAfter;
        cpuPidBefore=cpuPidAfter;

        return usage;
    }

    public long getCPUTotal()
    {
        long cpuTotal=0;
        String result;
        String[] returnString;

        try {
            Process process = Runtime.getRuntime().exec("cat /proc/stat");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream.readLine();
            inputstream.close();
            while(result.equals(""))
            {
                result=inputstream.readLine();
            }
            Log.d("SystemSet","cpu Usage:"+result);
            returnString=result.split(" ");
            for(int i=1; i<returnString.length; i++)
            {
                if(!returnString[i].equals(""))
                    cpuTotal+= Long.valueOf(returnString[i]);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return cpuTotal;
    }

    public long getCPUPid()
    {
        long cpuPid = 0;

        String result;
        String[] returnString;

        try {
            Process process = Runtime.getRuntime().exec("cat /proc/"+app.PID+"/stat");
			Log.d("SystemSet1", "App id is "+Integer.toString(app.PID));
            if(process==null)
                return 0;
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream.readLine();
            while(result==null)
            {
                inputstream.readLine();
//                Log.d("UtilCollector","cannot get proc stat");
            }
            while(result.equals(""))
            {
                result=inputstream.readLine();
            }
            Log.d("SystemSet","cpu Usage:"+result);
            inputstream.close();
            returnString=result.split(" ");
            cpuPid= Long.valueOf(returnString[13])+ Long.valueOf(returnString[14]);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return cpuPid;
    }

/*    public double gpuUsage()
    {
        String result = null;
        String[] returnString, gpudata;
        double usage=0;
        gpudata=new String[2];
        Process process;

        try {
            process = Runtime.getRuntime().exec("cat /sys/class/kgsl/kgsl-3d0/gpubusy");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream.readLine();

            while(result.equals(""))
            {
                result=inputstream.readLine();
            }
            Log.d("SystemSet","gpu Usage:"+result);
            inputstream.close();
//			Log.d("PhaseService", result);
            returnString=result.split(" ");
            int j=0;
            for(int i=0; i<returnString.length; i++)
            {
                if(!returnString[i].equals(""))
                    gpudata[j++]=returnString[i];
            }
//			Log.d("PhaseService", gpudata[0]+", "+gpudata[1]);
            if(Integer.valueOf(gpudata[1])!=0)
                usage= Double.valueOf(gpudata[0])/ Integer.valueOf(gpudata[1]);
            else
                usage=0;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return usage;
    }*/

    public double gpuUsage()
    {
        String result = null;
        String[] returnString, gpudata;
        double usage=0;
        gpudata=new String[2];
        Process process;

        try {
            process = Runtime.getRuntime().exec("cat /sys/class/kgsl/kgsl-3d0/gpubusy");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream.readLine();

            while(result.equals(""))
            {
                result=inputstream.readLine();
            }
            inputstream.close();
//			Log.d("PhaseService", result);
            returnString=result.split(" ");
            int j=0;
            for(int i=0; i<returnString.length; i++)
            {
                if(!returnString[i].equals(""))
                    gpudata[j++]=returnString[i];
            }
//			Log.d("PhaseService", gpudata[0]+", "+gpudata[1]);
            if(Integer.valueOf(gpudata[1])!=0)
                usage=Double.valueOf(gpudata[0])/Integer.valueOf(gpudata[1]);
            else
                usage=0;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return usage;
    }

    @SuppressLint("NewApi")
    public double memUsage_App()
    {
        double usedmem = 0;
        double totalmem=0;
        String result;
        String[] result_arr;
/*		ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		MemoryInfo memoryInfo=new MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);*/

        Process process;
        try {
            process = Runtime.getRuntime().exec("cat /proc/"+app.PID+"/stat");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream.readLine();

            if(result==null)
            {
                inputstream.close();
                return 0;
            }

            while(result.equals(""))
            {
                result=inputstream.readLine();
            }
            Log.d("SystemSet","mem Usage:"+result);
            inputstream.close();
            usedmem= Double.valueOf(result.split(" ")[23]);

            process = Runtime.getRuntime().exec("cat /proc/meminfo");
            BufferedReader inputstream1=new BufferedReader(new InputStreamReader(process.getInputStream()));

            result=inputstream1.readLine();

            while(result.equals(""))
            {
                result=inputstream1.readLine();
            }
            Log.d("SystemSet","mem Usage:"+result);
            inputstream1.close();
            result_arr=result.split(" ");
            for(int i=1;i<result_arr.length;i++)
            {
                if(!result_arr[i].equals(""))
                    {
                    totalmem= Double.valueOf(result_arr[i]);
                    break;
                }
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

//		usage=(usage*4)/memoryInfo.totalMem;
        return 4*usedmem/totalmem;
    }
}
