package com.example.xxz140830.phaseidentify;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

public class PhaseControler extends TimerTask {

	WriteFile writefile;
	String appname;
	Timer timer=null, timerwrite;
	TimerTask timertask=null, timerwritetask;
	double cpuUse, gpuUse, memUse;
	long cpuTotalBefore=0;
	long cpuPidBefore=0;
	int Freq;
	Queue<String> file;
	Queue<Double> cpuUsageQ, gpuUsageQ, memUsageQ;
	Phase curPhase;
	AppEntity app;
	Core core;
	int changecounter=0;
	
	int timeCounter=0;
	int timeCounter1=0;
	
	public PhaseControler(AppEntity app, Core core, WriteFile writefile)
	{
		this.app=app;
		this.core=core;
		
		Log.d("PhaseControl", "In the instructor");
		cpuUsageQ=new LinkedBlockingQueue<Double>();
		gpuUsageQ=new LinkedBlockingQueue<Double>();
		memUsageQ=new LinkedBlockingQueue<Double>();
		file=new LinkedBlockingQueue<String>();
		this.writefile=writefile;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Log.d("PhaseControl", "timeCounter: " + timeCounter);
		
		readPhase(cpuUse, gpuUse, memUse);
		timeCounter++;
		timeCounter1++;
		
		detectPhase1(cpuUse, gpuUse, memUse);
		
		if(timeCounter1==1)
		{
		int i=0;
		timeCounter1=0;
		for(; i<app.phases.length; i++)
		{
			if(identifyPhase(cpuUse, gpuUse, memUse, app.phases[i])==true)
			{
//				Log.d("PhaseService", "break i: "+i);
				break;
				}
		}
//		file.add("break i: "+i);
		if(i<app.phases.length)
		{
		if(curPhase==null)
		{
//			Log.d("PhaseControl", "Loading Phase");
			
			/*remove when using dummy app*/
//			loadPhase(app.phases[i]);
			
//			writefile.write("Setting Freq".split("#"));
			for(int j=0;j<app.phases.length;j++)
				app.phases[j].count=0;
			app.phases[i].count++;
		}
		else
			if(!curPhase.equals(app.phases[i]))
			{
//				Log.d("PhaseControl", "Loading Phase");
				changecounter++;
				if(changecounter>2)
				{
					
					/*remove when using dummy app*/
//					loadPhase(app.phases[i]);
					
					file.add("Setting Freq#"+i);
				}
				for(int j=0;j<app.phases.length;j++)
					app.phases[j].count=0;
				app.phases[i].count++;
			}else
			{
				changecounter=0;
			}
		}
		}
		if(timeCounter==50)
		{
			timeCounter=0;
			
			while(!file.isEmpty())
			{
				String string=file.remove();
//				Log.d("PhaseControl", string);
				writefile.write(string.split("#"));
			}
		}
	}
	
	public void loadPhase(Phase phase)
	{
		curPhase=phase;
		app.AppType=phase.AppType;
		

		app.Deviation.clear();
		
		SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
		core.Frequency=app.PreferFreq;
	}

	public void readPhase(double cpuUsage, double gpuUsage, double memUsage)
	{
		gpuUse=gpuUsage();
		cpuUse=cpuUsage_App();
		memUse=memUsage_App();
		
		Process process;
		String result = null;
		try {
			process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			result=inputstream.readLine();
			while(result.equals(""))
				result=inputstream.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		file.add(cpuUse+"#"+gpuUse+"#"+memUse+"#"+result+"#"+core.CoreNumber+"#"+ SystemClock.uptimeMillis()+"#"+changecounter);
		
		cpuUsageQ.add(cpuUsage);
		gpuUsageQ.add(gpuUsage);
		memUsageQ.add(memUsage);
	}
	
	public void detectPhase1(double cpuUsage, double gpuUsage, double memUsage)
	{
		cpuUsage=0;
		gpuUsage=0;
		memUsage=0;
		int size_cpu=cpuUsageQ.size();
		int size_gpu=gpuUsageQ.size();
		int size_mem=memUsageQ.size();
		
		while(!cpuUsageQ.isEmpty())
			cpuUsage+=cpuUsageQ.remove();
		cpuUsage=cpuUsage/size_cpu;
		
		while(!gpuUsageQ.isEmpty())
			gpuUsage+=gpuUsageQ.remove();
		gpuUsage=gpuUsage/size_gpu;
		
		while(!memUsageQ.isEmpty())
			memUsage+=memUsageQ.remove();
		memUsage=memUsage/size_mem;
	}
	
	public boolean identifyPhase(double cpuUsage, double gpuUsage, double memUsage, Phase phase)
	{
		Log.d("PhaseService", "identifyPhase " + cpuUsage + " " + gpuUsage + " " + memUsage);
		Log.d("PhaseService", "(" + phase.cpuUsage_min + ", " + phase.cpuUsage_max + ")  " + "(" + phase.gpuUsage_min + ", " + phase.gpuUsage_max + ")  (" + phase.memUsage_min + ", " + phase.memUsage_max + ")");
		if(cpuUsage<phase.cpuUsage_max&&cpuUsage>=phase.cpuUsage_min)
			if(gpuUsage<phase.gpuUsage_max&&gpuUsage>=phase.gpuUsage_min)
				if(memUsage<phase.memUsage_max&&memUsage>=phase.memUsage_min)
				{
					Log.d("PhaseService", "return true");
					return true;
				}
		return false;
	}
	
	public Double cpuUsage_App()
	{
		long cpuTotalAfter;
		long cpuPidAfter;
		
		double usage;
		
		if(cpuTotalBefore==0||cpuPidBefore==0)
		{
			cpuTotalBefore=getCPUTotal();
			cpuPidBefore=getCPUPid();
			return 0.00;
		}
		
		cpuTotalAfter=getCPUTotal();
		cpuPidAfter=getCPUPid();
		
		usage=(double)(cpuPidAfter-cpuPidBefore)/(cpuTotalAfter-cpuTotalBefore);
		cpuTotalBefore=cpuTotalAfter;
		cpuPidBefore=cpuPidAfter;
		
		return usage;
	}
	
	public long getCPUTotal()
	{
		long cpuTotal=0;
		String result;
		String[] returnString;
		
		try {
			Process process = Runtime.getRuntime().exec("cat /proc/stat");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			result=inputstream.readLine();
			inputstream.close();
			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			returnString=result.split(" ");
			for(int i=1; i<returnString.length; i++)
			{
				if(!returnString[i].equals(""))
					cpuTotal+= Long.valueOf(returnString[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cpuTotal;
	}
	
	public long getCPUPid()
	{
		long cpuPid = 0;
		
		String result;
		String[] returnString;
		
		try {
			Process process = Runtime.getRuntime().exec("cat /proc/"+app.PID+"/stat");
//			Log.d("PhaseControler", Integer.toString(app.PID));
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			result=inputstream.readLine();
			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			inputstream.close();
			returnString=result.split(" ");
			cpuPid= Long.valueOf(returnString[13])+ Long.valueOf(returnString[14]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cpuPid;
	}
	
	public double gpuUsage()
	{
		String result = null;
		String[] returnString, gpudata;
		double usage=0;
		gpudata=new String[2];
		Process process;
		
		try {
			process = Runtime.getRuntime().exec("cat /sys/class/kgsl/kgsl-3d0/gpubusy");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			result=inputstream.readLine();
			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			inputstream.close();
			Log.d("PhaseService", result);
			returnString=result.split(" ");
			int j=0;
			for(int i=0; i<returnString.length; i++)
			{
				if(!returnString[i].equals(""))
					gpudata[j++]=returnString[i];
			}
			Log.d("PhaseService", gpudata[0] + ", " + gpudata[1]);
			if(Integer.valueOf(gpudata[1])!=0)
				usage= Double.valueOf(gpudata[0])/ Integer.valueOf(gpudata[1]);
			else
				usage=0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usage;
	}
	
	@SuppressLint("NewApi")
	public double memUsage_App()
	{
		double usedmem = 0;
		double totalmem=0;
		String result;
		String[] result_arr;
/*		ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		MemoryInfo memoryInfo=new MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);*/
		
		Process process;
		try {
			process = Runtime.getRuntime().exec("cat /proc/"+app.PID+"/stat");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			result=inputstream.readLine();
			
			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			inputstream.close();
			usedmem= Double.valueOf(result.split(" ")[23]);
			
			process = Runtime.getRuntime().exec("cat /proc/meminfo");
			BufferedReader inputstream1=new BufferedReader(new InputStreamReader(process.getInputStream()));

			result=inputstream1.readLine();
			
			while(result.equals(""))
			{
				result=inputstream1.readLine();
			}
			inputstream1.close();
			result_arr=result.split(" ");
			for(int i=1;i<result_arr.length;i++)
			{
				if(!result_arr[i].equals(""))
				{
					totalmem= Double.valueOf(result_arr[i]);
					break;
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		usage=(usage*4)/memoryInfo.totalMem;
		return 4*usedmem/totalmem;
	}
}
