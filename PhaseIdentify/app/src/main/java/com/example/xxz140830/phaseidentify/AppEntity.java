package com.example.xxz140830.phaseidentify;

import android.os.SystemClock;
import android.util.Log;

import java.util.Queue;
import java.util.Timer;
import java.util.concurrent.LinkedBlockingQueue;

public class AppEntity {
	public String AppName;
	public String AppVideoTest;
	public String ServiceName;
	public long releaseTime;
	public int PID;
	public int AppType;
	public int PreferFreq;
	public Core core;
	public Queue<Float> Deviation;
	public double sweetSpotPerformance;
	public double performance;
	
	public double Deadline;
	public double ExeTime; 
	public Phase[] phases;
	public int multiple_phase=0;
	public PhaseControlClassifier phasecontroler;
	public String trainingDataPath;
	public Timer timer;
	
	public WriteFile writefile;
	
	public AppEntity(String appname, String videotest, int apptype, int preferfreq, String servicename, double sweetSpotPerformance)
	{
		this.AppName=appname;
		this.PID=-1;
		this.AppType=apptype;
		this.PreferFreq=preferfreq;
		this.ServiceName=servicename;
		this.sweetSpotPerformance=sweetSpotPerformance;
		this.AppVideoTest=videotest;
		Deviation=new LinkedBlockingQueue<Float>();
		
		Log.d("AppEntity", "init app entity of " + appname);
//		this.writefile=new WriteFile(appname+".csv");
//		writefile.write("Core Number#Core Frequency#Sweet Performance#Performance#Deviation#Measured Frequency".split("#"));
	}
	
	public AppEntity(String appname, int apptype, int preferfreq, String servicename, double sweetSpotPerformance, double Deadline, double ExeTime)
	{
		this.AppName=appname;
		this.PID=-1;
		this.AppType=apptype;
		this.PreferFreq=preferfreq;
		this.ServiceName=servicename;
		this.sweetSpotPerformance=sweetSpotPerformance;
		this.Deadline=Deadline;
		this.ExeTime=ExeTime;
		this.releaseTime= SystemClock.uptimeMillis();
		Deviation=new LinkedBlockingQueue<Float>();
		
		this.writefile=new WriteFile(appname+".csv");
		writefile.write("Core Number#Core Frequency#Sweet Performance#Performance#Deviation#Measured Frequency".split("#"));
	}
	
	public AppEntity(String appname, String appvideotest, int apptype, int preferfreq, double sweetSpotPerformance)
	{
		this.AppName=appname;
		this.AppVideoTest=appvideotest;
		this.PID=-1;
		this.AppType=apptype;
		this.PreferFreq=preferfreq;
		this.sweetSpotPerformance=sweetSpotPerformance;
		Deviation=new LinkedBlockingQueue<Float>();
		
		this.writefile=new WriteFile(appname+".csv");
		writefile.write("Core Number#Core Frequency#Sweet Performance#Performance#Deviation#Measured Frequency".split("#"));
	}
	
	public AppEntity(String appname, int apptype, int preferfreq, double sweetSpotPerformance)
	{
		this.AppName=appname;
		this.PID=-1;
		this.AppType=apptype;
		this.PreferFreq=preferfreq;
		this.sweetSpotPerformance=sweetSpotPerformance;
		Deviation=new LinkedBlockingQueue<Float>();
		this.writefile=new WriteFile(appname+".csv");
		writefile.write("Core Number#Core Frequency#Sweet Performance#Performance#Deviation#Measured Frequency".split("#"));
	}
	
	public void CalDeviation(float performance)
	{
		this.performance=performance;
		 float Dev=((float)performance/(float)sweetSpotPerformance)-1;
		 Log.d("AppEntity", "performance: " + performance + "sweetSpotPerformance: " + sweetSpotPerformance);
//		 if(Dev<0.9)
			 Deviation.add(Dev);
/*		 if(AppType==Apptype.LATENCYAPP)
			 Deadline=Deadline-performance;*/
		 if(Deviation.size()==6)
			 Deviation.remove();
	}
}
