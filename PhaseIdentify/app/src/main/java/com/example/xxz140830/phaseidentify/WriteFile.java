package com.example.xxz140830.phaseidentify;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java_cup.Main;

public class WriteFile {

	CSVWriter csvwriter;
	String csvpath;
	
	WriteFile(String csvpath)
	{
		this.csvpath=csvpath;
		try {

			Log.d("WriteFile", "before newing filewriter: "+csvpath);
			File heapFile = new File(MainActivity.filepath, csvpath); //please add new path
			Log.d("WriteFile", "before newing csvwriter: "+heapFile.getPath()+" "+MainActivity.filepath.getPath());
			FileWriter filewriter=new FileWriter(heapFile);
			csvwriter=new CSVWriter(filewriter);
			Log.d("WriteFile", "after newing csvwriter");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void write(String[] content)
	{
		Log.d("WriteFile", "Writing the file");
		csvwriter.writeNext(content);
	}
	
	void close()
	{
		try {
			csvwriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
