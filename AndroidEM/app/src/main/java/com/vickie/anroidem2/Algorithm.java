package com.vickie.anroidem2;

import android.content.Context;
import android.util.Log;

public class Algorithm {
	
	static int CoreCounter=0;
	public static Context context;
	
	public static void MappingToCore(AppEntity app)
	{
		CoreCounter=CoreCounter%4;
		if(app.AppType==Apptype.VIDEOAPP)
		{
			CoreManagement.core[CoreCounter].VideoOccupation=true;
			CoreManagement.core[CoreCounter].queue.add(app);
//			Thread apptocore=new Thread(new AppToCore(CoreManagement.core[CoreCounter], context));
//			apptocore.start();
			Log.d("Algorithm", "Mapping video app to core");
			CoreCounter++;
			return;
		}
		if(CoreManagement.core[CoreCounter].VideoOccupation==true)
		{
			CoreCounter++;
			CoreCounter=CoreCounter%4;
		}
		CoreManagement.core[CoreCounter].queue.add(app);
//		Thread apptocore=new Thread(new AppToCore(CoreManagement.core[CoreCounter], context));
//		apptocore.start();
		Log.d("Algorithm", "Mapping latency app to core");
		CoreCounter++;
		return;
	}
	
	public static void MappingOfFreq(AppEntity app, Core core)
	{
		int pid=0;
		
		Log.d("Algorithm", "The core number "+Integer.toString(core.CoreNumber)+"; The frequency "+Integer.toString(app.PreferFreq)+"; The PID "+Integer.toString(app.PID));
		SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
/*		pid=SystemSet.setcpuAffinity(app.PID, core.CoreNumber);
		Log.d("Algorithm", "The returned PID is "+Integer.toString(pid));*/
//		SystemSet.setCPUAffinity(app.PID, 3);
	}
}
