package com.vickie.anroidem2;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class StartScheduler extends Service {

	public int onStartCommand(Intent intent, int flags, int startId)
	{
		String appname=intent.getStringExtra("AppName");
		AppEntity app = null;
		Core core = null;
		Log.d("StartScheduler", "appname: "+appname);
		
		for(int i=0; i<4;i++)
		{
			Log.d("StartScheduler", "0");
			if(CoreManagement.core[i].app==null)
				Log.d("StartScheduler", "there is no app on "+CoreManagement.core[i].CoreNumber);
			if(CoreManagement.core[i].app!=null)
			{
				if(CoreManagement.core[i].app.AppName.equals(appname))
				{
					Log.d("StartScheduler", "1");
					core=CoreManagement.core[i];
//					SystemSet.setCPUFreq(2265600, core.CoreNumber);
					Log.d("StartScheduler", "2");
					app=core.app;
					if(app.multiple_phase==1&&MainActivity.energysaving==1)
					{
						app.timer.cancel();
						while(!app.phasecontroler.file.isEmpty())
						{
							String string=app.phasecontroler.file.remove();
							Log.d("PhaseControl", string);
							app.phasecontroler.writefile.write(string.split("#"));
						}
						app.phasecontroler.writefile.close();
						Log.d("StartScheduler", "Timer canceled");
					}else{
						app.timer.cancel();
					}
					if(app.AppType==Apptype.LATENCYAPP)
					{
						long endTime=SystemClock.uptimeMillis();
						Log.d("StartScheduler", app.AppName+" "+(((double)app.releaseTime+app.Deadline)-(double)endTime));
						Log.d("StartScheduler", Long.toString(app.releaseTime));
						Log.d("StartScheduler", Long.toString(endTime));
						Log.d("StartScheduler", Double.toString(app.Deadline));
						app.writefile.write(" # ".split("#"));
						app.writefile.write("Release Time#Deadline#End Time".split("#"));
						app.writefile.write((app.releaseTime+"#"+app.Deadline+"#"+endTime).split("#"));
					}
//					app.writefile.write(("EndTime#"+SystemClock.uptimeMillis()).split("#"));
					app.writefile.write(" # ".split("#"));
					app.writefile.close();
					Log.d("StartScheduler", "file closed");
					AppManagement.finishedapp++;
					
					if(AppManagement.finishedapp==13)
					{
						MainActivity.emtimer.Close();
				/*		AppEntity app1;
						while(Scheduler.startedBusyApp.isEmpty()==false)
						{
							app1=Scheduler.startedBusyApp.remove();
							SystemSet.stopApp(app1, Scheduler.context);
						}*/
					}
					break;
				}
			}
//			Log.d("StartScheduler", "core appname:"+CoreManagement.core[i].app.AppName);
		}
		
		if(app==null || core==null)
		{
			Log.d("StartScheduler", "Error");
			return 0;
		}
		
		if(app.AppType==Apptype.GAMEAPP||app.AppType==Apptype.VIDEOAPP||app.AppType==Apptype.UIAPP)
			Scheduler.AppRunning=false;
		core.app=null;
		if(AppManagement.finishedapp<1)
		{
			Thread scheduler=new Thread(new Scheduler(core));
			scheduler.start();
		}
		return START_NOT_STICKY;
		}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stube
			
		return null;
	}

}
