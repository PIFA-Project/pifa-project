package com.vickie.anroidem2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import android.util.Log;

public class EMTimer {

		Timer timer=null;
		TimerTask timerTask=null;
		WriteFile writefile=null;
	
	EMTimer()
	{
		timer=new Timer();
		
		writefile=new WriteFile("FreqRecord.csv");
		
		timerTask = new TimerTask(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				DataOutputStream outputstream=new DataOutputStream(SystemSet.suCPU.getOutputStream());
				BufferedReader inputstream=new BufferedReader(new InputStreamReader(SystemSet.suCPU.getInputStream()));
				
				String[] freqcpu=new String[4];
				
				try {
					for(int i=0;i<4;i++)
					{
						outputstream.writeBytes("cat /sys/devices/system/cpu/cpu"+i+"/cpufreq/scaling_cur_freq\n");
						outputstream.flush();
						
						do{
							freqcpu[i]=inputstream.readLine();
							if(freqcpu[i].length()>10)
								freqcpu[i]=null;
							}while(freqcpu[i]==null);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				Log.d("EMTime", freqcpu[0]+"#"+freqcpu[1]+"#"+freqcpu[2]+"#"+freqcpu[3]);
				writefile.write((freqcpu[0]+"#"+freqcpu[1]+"#"+freqcpu[2]+"#"+freqcpu[3]).split("#"));
//				Log.d("EMTime", "After write file");
			}
			
		};
	}
	
	void StartTimer()
	{
		if(timer!=null&&timerTask!=null&&writefile!=null)
			timer.schedule(timerTask, 0, 1000);
	}
	
	void Close()
	{
		if(timer!=null&&timerTask!=null&&writefile!=null)
		{
			timer.cancel();
			writefile.close();
		}
	}
	
}
