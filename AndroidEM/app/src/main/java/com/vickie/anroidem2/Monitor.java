/*
package com.vickie.anroidem2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class Monitor extends Service {
	
	AppEntity app;
	Core core;
	Iterator<Float> iterator;
	String[] writecontent;
	String writestring;
	String output=null;
	int phasenum;
	int calculateflag;
		
	public int onStartCommand(Intent intent, int flags, int startId)
	{

		String appname=intent.getStringExtra("AppName");
		float performance=intent.getFloatExtra("Performance", (float) 0.0);
		if(Float.isNaN(performance))
			performance=0;
		phasenum=intent.getIntExtra("PhaseNumber", -1);
		Log.d("Monitor", "AppName: "+appname);
		Log.d("Monitor", "Performance: "+appname+" "+performance);
		Log.d("Monitor", "PhaseNumber: "+phasenum);
		calculateflag=1;
		if(performance==0||Float.isInfinite(performance))
			return START_NOT_STICKY;
		
		for(int i=0; i<4;i++)
		{
			if(CoreManagement.core[i].app!=null)
			{
				if(CoreManagement.core[i].app.AppName.equals(appname))
				{
					app=CoreManagement.core[i].app;
					core=CoreManagement.core[i];
					app.CalDeviation(performance);
					if(app.AppType==Apptype.LATENCYAPP)
						controlService(app, CoreManagement.core[i]);
					if(app.AppType==Apptype.GAMEAPP)
						controlGame(app, CoreManagement.core[i]);
					if(app.AppType==Apptype.UIAPP)
						controlUI(app, CoreManagement.core[i]);
					if(app.AppType==Apptype.VIDEOAPP)
						controlVideo(app, CoreManagement.core[i]);
					
*/
/*					if(app.multiple_phase==1&&MainActivity.phasecontrol==1)
						app.phases[phasenum].freq=CoreManagement.core[i].Frequency;*//*

					
					break;
				}
			}
		}
		
*/
/*		Intent intent1=new Intent();
		intent1.setClassName("com.example.matrixapp1", "com.example.matrixapp1.MatrixCalculate");
		startService(intent1);*//*

		
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
		Log.d("Monitor", "Monitor is ending");
		return START_NOT_STICKY;
	}


	void controlVideo(AppEntity app, Core core)
	{
		int Freq = 0;
		float Dev=0;
		
		iterator=app.Deviation.iterator();
		while(iterator.hasNext())
			Dev=Dev+(Float) iterator.next();
		Dev=Dev/app.Deviation.size();
		Log.d("Monitor", "Deviation: "+Dev);
		if(MainActivity.energysaving==1)
		{
		if(Dev>0.1)
		{
			Freq=SystemSet.getHigherFreq(core.Frequency);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
		}
		else if(Dev<-0.1)
		{
			if(core.Frequency>app.PreferFreq)
			{
			Freq=SystemSet.getLowerFreq(core.Frequency);
			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}
		}
		}
		Log.d("Monitor", "Frequency of core "+ core.CoreNumber+" "+core.Frequency);
//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
*/
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*//*

		
		
		try {
			Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			while((output=inputstream.readLine())!=null){
    			break;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
		app.writefile.write(output.split("#"));
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
	}
	
	void controlUI(AppEntity app, Core core)
	{
		int Freq;
		float Dev=0;
		
		iterator=app.Deviation.iterator();
		while(iterator.hasNext())
			Dev=Dev+(Float) iterator.next();
		Dev=Dev/app.Deviation.size();
		Log.d("Monitor", "Deviation: "+Dev);
			
		if(MainActivity.energysaving==1)
		{
		if(Dev>0.1)
		{
			Freq=SystemSet.getHigherFreq(core.Frequency);
			if(app.multiple_phase==1&&MainActivity.phasecontrol==1&&phasenum!=-1)
				app.phases[phasenum].freq=Freq;
			else{
				SystemSet.setCPUFreq(Freq, core.CoreNumber);
				core.Frequency=Freq;
			}
			
			*/
/*if(app.multiple_phase!=1)
			{
			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}else
				app.phases[phasenum].freq=Freq;*//*

		}
		else if(Dev<-0.1)
		{
			if(core.Frequency>app.PreferFreq)
			{
				
			Freq=SystemSet.getLowerFreq(core.Frequency);
			if(app.multiple_phase==1&&MainActivity.phasecontrol==1&&phasenum!=-1)
				app.phases[phasenum].freq=Freq;
			else{
				SystemSet.setCPUFreq(Freq, core.CoreNumber);
				core.Frequency=Freq;
			}
			*/
/*if(app.multiple_phase!=1)
			{
			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}else
				app.phases[phasenum].freq=Freq;*//*

			}
		}//else
//			SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
		}
		
//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
*/
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*//*

		
		try {
			Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			while((output=inputstream.readLine())!=null){
    			break;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
		app.writefile.write(output.split("#"));
		
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
	}
	
	void controlGame(AppEntity app, Core core)
	{
		int Freq;
		float Dev=0;
		
		iterator=app.Deviation.iterator();
		while(iterator.hasNext())
			Dev=Dev+(Float) iterator.next();
		Dev=Dev/app.Deviation.size();
		
		if(app.Deviation.size()==0)
			return;
		
		Log.d("Monitor", "Deviation: "+Dev);
		
		Log.d("Monitor", "Core Freq: "+core.Frequency);
		if(MainActivity.energysaving==1)
		{
		if(Dev>0.2)
		{
			Freq=SystemSet.getHigherFreq(core.Frequency);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
		}
		else if(Dev<0)
		{
			if(core.Frequency>app.PreferFreq)
			{
			Freq=SystemSet.getLowerFreq(core.Frequency);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}
		}//else
//			SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
		}

//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
*/
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*//*

		
		
		try {
			Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			while((output=inputstream.readLine())!=null){
    			break;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
		app.writefile.write(output.split("#"));
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
	}
	
	void controlService(AppEntity app, Core core)
	{
		int Freq;
		float Dev=0;
		
		iterator=app.Deviation.iterator();
		while(iterator.hasNext())
			Dev=Dev+(Float) iterator.next();
		Dev=Dev/app.Deviation.size();
		Log.d("Core Frequency", Integer.toString(core.Frequency));
		Log.d("Deviation ", Float.toString(Dev));
		if(MainActivity.energysaving==1)
		{
		if(Dev>0.1)
		{
			Freq=SystemSet.getHigherFreq(core.Frequency);
			Log.d("Monitor", "Before setting freq of cpu"+core.CoreNumber);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			Log.d("Monitor", "After setting freq of cpu"+core.CoreNumber);
			core.Frequency=Freq;
		}
		else if(Dev<-0.1)
		{
			if(core.Frequency>app.PreferFreq)
			{
			Freq=SystemSet.getLowerFreq(core.Frequency);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}
		}
		}

//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
*/
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*//*

		
		try {
			Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			while((output=inputstream.readLine())!=null){
    			break;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("Monitor", "2");
		output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
		app.writefile.write(output.split("#"));
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
*/
/*	public class writeThread implements Runnable{

		String content;
		
		writeThread(String content)
		{
			this.content=content;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			DataOutputStream outputstream=new DataOutputStream(SystemSet.suCPU.getOutputStream());
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(SystemSet.suCPU.getInputStream()));
			String output = null;
			
			try {
				outputstream.writeBytes("busybox cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_max_freq\n");
				outputstream.flush();
				
				while((output=inputstream.readLine())!=null){
	    			break;
	    		}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			output=content+"#"+output;
			app.writefile.write(output.split("#"));
		}
		
	}*//*

}
*/
