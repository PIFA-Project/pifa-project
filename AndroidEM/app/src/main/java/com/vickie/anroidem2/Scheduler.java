package com.vickie.anroidem2;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

public class Scheduler implements Runnable {

	public static boolean AppRunning;
	public Core core;
	public static Context context;
	public static Queue<AppEntity> startedBusyApp;
	
	public Scheduler(Core core)
	{
		this.core=core;
	}
	
	static public void SchedulerInit(Context context)
	{
		AppRunning=false;
		Scheduler.context=context;
		startedBusyApp=new LinkedBlockingQueue<AppEntity>();
	}
	
	static public void ServicePriotize()
	{
		double Utilization=0, Umax=3;
		double remainingTime;
		int minFreq, pointer, Freq;
		AppEntity[] appArray=new AppEntity[AppManagement.Queue2.size()];
		int i=0;
		for(i=0;i<appArray.length;i++)
			appArray[i]=AppManagement.Queue2.remove();
		
		if(MainActivity.energysaving==1&&appArray.length!=0)
		{
		while(true)
		{
			minFreq=appArray[0].PreferFreq;
			pointer=0;
			for(i=0;i<appArray.length;i++)
			{
				if(minFreq>appArray[i].PreferFreq)
				{
					minFreq=appArray[i].PreferFreq;
					pointer=i;
				}
				
				Utilization+=appArray[i].ExeTime/appArray[i].Deadline;
			}
			
			Log.d("Scheduler", "Minimal Frequency: "+minFreq);
			Log.d("Scheduler", "Utilization: "+Utilization);
			
			if(Utilization>Umax)
			{
				remainingTime=appArray[0].Deadline;
				for(i=0;i<appArray.length;i++)
				{
					if(minFreq==appArray[i].PreferFreq)
					{
						if(remainingTime>appArray[i].Deadline)
							pointer=i;
					}
				}
				
				Freq=appArray[pointer].PreferFreq;
				appArray[pointer].PreferFreq=SystemSet.getHigherFreq(Freq);
				Log.d("Scheduler", "App: "+appArray[pointer].AppName+" Freq: "+appArray[pointer].PreferFreq);
				appArray[pointer].sweetSpotPerformance=((double)Freq/(double)appArray[pointer].PreferFreq)*appArray[pointer].sweetSpotPerformance;
				appArray[pointer].ExeTime=((double)Freq/(double)appArray[pointer].PreferFreq)*appArray[pointer].ExeTime;
				Log.d("Scheduler", "SweetSpot: "+appArray[pointer].sweetSpotPerformance+" ExeTime: "+appArray[pointer].ExeTime);
			}
			else
				break;
			
			Utilization=0;
		}
		}
		
		AppEntity tmp;
		for(i=0;i<appArray.length;i++)
			for(int j=i+1;j<appArray.length;j++)
				if(appArray[i].Deadline>appArray[j].Deadline)
				{
					tmp=appArray[i];
					appArray[i]=appArray[j];
					appArray[j]=tmp;
				}
				
		for(AppEntity e : appArray)
		{
			AppManagement.Queue2.add(e);
			Log.d("Scheduler", e.AppName+" "+e.ExeTime);
			Log.d("Scheduler", ""+e.Deadline);
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		AppEntity app;
	
		Log.d("Scheduler", "The schedulr thread just starts: core number "+core.CoreNumber);
		SystemSet.setCPUFreq(300000, core.CoreNumber);
		synchronized(context)
		{
		
		if(AppRunning==false)
		{
//			Log.d("Scheduler", "AppRunning false");
			if(AppManagement.Queue1.isEmpty()==false)
			{
				app=AppManagement.Queue1.remove();
//				app.writefile.write(("StartTime#"+SystemClock.uptimeMillis()).split("#"));
				Log.d("Scheduler", "before starting app: "+app.AppName);
				SystemSet.runApp(app, core, context);
				CoreManagement.core[core.CoreNumber].app=app;
				Log.d("Scheduler", "core: "+core.CoreNumber+" core app: "+core.app.AppName+" app: "+app.AppName);
				AppRunning=true;
			}
			else
			{
				if(AppManagement.Queue2.isEmpty()==false)
				{
					app=AppManagement.Queue2.remove();
					
//					app.writefile.write(("StartTime#"+SystemClock.uptimeMillis()).split("#"));
					SystemSet.runApp(app, core, context);
					
					SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
					CoreManagement.core[core.CoreNumber].app=app;
					Log.d("Scheduler", "core: "+core.CoreNumber+" core app: "+core.app.AppName+" app: "+app.AppName);
				}
				else
				{
//					runBusyService(core);
					SystemSet.setCPUFreq(300000, core.CoreNumber);
//					shutoffCore(core);
				}
			}
		}
		else
		{
//			Log.d("Scheduler", "AppRunning true");
			if(AppManagement.Queue2.isEmpty()==false)
			{
				app=AppManagement.Queue2.remove();
				SystemSet.runApp(app, core, context);
				SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
				CoreManagement.core[core.CoreNumber].app=app;
				Log.d("Scheduler", "core: "+core.CoreNumber+" core app: "+core.app.AppName+" app: "+app.AppName);
			}
			else
			{
//				runBusyService(core);
//				SystemSet.setCPUFreq(300000, core.CoreNumber);
//				Log.d("Scheduler", "busyService is started");
//				shutoffCore(core);
			}
		}
		}
	}
	
	@SuppressLint("NewApi")
	void shutoffCore(Core core)
	{
		core.on=false;
		for(int i=0; i<4; i++)
			if(i!=core.CoreNumber&&CoreManagement.core[i].on==true)
				SystemSet.setCPUAffinity(android.system.Os.getpid(), i);
		SystemSet.shutoffCore(core);
	}
	
	void runBusyService(Core core)
	{
		if(AppManagement.BusyService.isEmpty()==false)
		{
			AppEntity app=AppManagement.BusyService.remove();
			SystemSet.runApp(app, core, context);
			startedBusyApp.add(app);
			Log.d("Scheduler", "busyService is started");
		}
	}

}
