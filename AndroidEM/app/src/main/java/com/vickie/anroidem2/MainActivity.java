package com.vickie.anroidem2;

import java.io.File;
import java.util.Random;

import android.support.v7.app.ActionBarActivity;
import android.widget.AdapterView;  
import android.widget.AdapterView.OnItemSelectedListener;  
import android.widget.ArrayAdapter;  
import android.widget.Spinner; 
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ActionBarActivity {
	
	Context context;
	Spinner spinner1;
	Spinner spinner2;
	Spinner spinner3;
	boolean governorflag;
	static int energysaving;  //0 Unknown, 1 Yes, 2 No
	static int phasecontrol;
	static File filedirectory;
	static EMTimer emtimer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		governorflag=false;
		energysaving=0;
		
		spinner1=(Spinner)findViewById(R.id.spinner1);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
		        R.array.EnergySaving, android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner1.setAdapter(adapter1);
		spinner1.setOnItemSelectedListener(new SpinnerEnergySaving());

		spinner2=(Spinner)findViewById(R.id.spinner2);
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
		        R.array.PowerGovernor, android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(adapter2);
		spinner2.setOnItemSelectedListener(new SpinnerPowerGovernor());
		
		spinner3=(Spinner)findViewById(R.id.spinner3);
		ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
		        R.array.PhaseControl, android.R.layout.simple_spinner_item);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner3.setAdapter(adapter3);
		spinner3.setOnItemSelectedListener(new SpinnerPhaseControl());
		
		Log.d("MainActivity", "OnCreate: "+System.currentTimeMillis());
		
	}

	public void setGovernor(View view)
	{
//		Governor=textGovernor.getText().toString();
//		Governor="androidem";
	}
	
	public void run(View view)
	{
		if(governorflag==true&&energysaving!=0)
		{
			Random r = new Random();
			int i1 = r.nextInt(9999999 - 1000000 + 1) + 1000000;
			Log.d("MainActivity", "New File");
			filedirectory=new File(Environment.getExternalStorageDirectory().getPath()+"/"+Integer.toString(i1)+"_"+"disablecore"+"_"+String.valueOf(spinner1.getSelectedItem())+"_"+String.valueOf(spinner2.getSelectedItem()));
			Log.d("MainActivity", "Make Directory");
			filedirectory.mkdirs();
			Log.d("MainActivity", "Before InitCoreManagement");
			CoreManagement.InitCoreManagement(4);
			context=this.getApplicationContext();
			Scheduler.SchedulerInit(context);
			Algorithm.context=context;
			
			Log.d("MainActivity", "Before InitAppManagement");
			AppManagement.InitAppManagement();
			Log.d("MainActivity", "After InitAppManagement");
			Scheduler.ServicePriotize();
			Log.d("MainActivity", "Prioritized");
			
			for(AppEntity appentity: AppManagement.Queue2)
			{
				Log.d("MainActivity", appentity.AppName);
				Log.d("MainActivity", "SweetSpot: "+appentity.sweetSpotPerformance+" ExeTime: "+appentity.ExeTime);
			}
//			emtimer=new EMTimer();
//			emtimer.StartTimer();
			for(int i=0;i<4;i++)
			{
				Thread scheduler=new Thread(new Scheduler(CoreManagement.core[i]));
				scheduler.start();
			}
		}
		else
			Log.d("MainActivity", "Governor or Energy Mode has not been set");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class SpinnerPowerGovernor implements OnItemSelectedListener 
	{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String string=String.valueOf(spinner2.getSelectedItem());
			if(!string.equals("none"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				SystemSet.setGovernor(string);
				governorflag=true;
			}
			else
				governorflag=false;
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}}
	
	public class SpinnerPhaseControl implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String string=String.valueOf(spinner3.getSelectedItem());
			if(string.equals("none"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				phasecontrol=0;
			}
			else if(string.equals("Yes"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				phasecontrol=1;
			}
			else if(string.equals("No"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				phasecontrol=2;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}}
	
	public class SpinnerEnergySaving implements OnItemSelectedListener 
	{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			String string=String.valueOf(spinner1.getSelectedItem());
			if(string.equals("none"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				energysaving=0;
			}
			else if(string.equals("Yes"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				energysaving=1;
			}
			else if(string.equals("No"))
			{
				Log.d("MainActivity", "onItemSelected "+string);
				energysaving=2;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}}
}
