package com.vickie.anroidem2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.util.Log;

import com.opencsv.CSVWriter;

public class WriteFile {

	CSVWriter csvwriter;
	String csvpath;
	
	WriteFile(String csvpath)
	{
		this.csvpath=csvpath;
		try {
			Log.d("WriteFile", "before newing filewriter");
			File heapFile = new File(MainActivity.filedirectory, csvpath);
			FileWriter filewriter=new FileWriter(heapFile);
			Log.d("WriteFile", "before newing csvwriter");
			csvwriter=new CSVWriter(filewriter);
			Log.d("WriteFile", "after newing csvwriter");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void write(String[] content)
	{
		Log.d("WriteFile", "Writing the file");
		csvwriter.writeNext(content);
	}
	
	void close()
	{
		try {
			csvwriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
