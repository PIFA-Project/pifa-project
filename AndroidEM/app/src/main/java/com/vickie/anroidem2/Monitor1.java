package com.vickie.anroidem2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Iterator;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.content.Context;

public class Monitor1 {

    AppEntity app;
    Core core;
    Iterator<Float> iterator;
    String[] writecontent;
    String writestring;
    String output=null;
    int phasenum;
    int calculateflag;
    static float prev_performance=0;

    public void onStartCommand(AppEntity app, int phasenum, Context context)
    {

        String line="";
        float performance;
        this.phasenum=phasenum;

        Log.d("Monitor","onStartCommand: "+phasenum);

        File sdcard = Environment.getExternalStorageDirectory();
/*        File file1 = new File(sdcard, "test.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file1));

            while ((line = br.readLine()) == null) Log.d("Monitor","line is none");
            Log.d("Monitor",line);
            br.close();
            if(line.equals("no"))
            {
                Intent intent2=new Intent();
                intent2.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.StartScheduler");
                intent2.putExtra("AppName", app.AppName);
                context.startService(intent2);

                FileOutputStream fileout=new FileOutputStream(file1);
                OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
                outputWriter.write("yes");
                outputWriter.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        if(phasenum!=-1) {
            if (app.phases[phasenum].AppType == Apptype.GAMEAPP || app.phases[phasenum].AppType == Apptype.VIDEOAPP) {
                File file = new File(sdcard, "fps.txt");
                Log.d("Monitor", "reading fps");
                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));

                    while ((line = br.readLine()) == null) ;
                    br.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                File file = new File(sdcard, "response.txt");
                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));

                    while ((line = br.readLine()) == null) ;
                    br.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            performance = Float.valueOf(line);
            if (Float.isNaN(performance))
                performance = 0;

            Log.d("Monitor", prev_performance+" "+performance);
            if(app.AppType==Apptype.UIAPP&&prev_performance==performance)
                return;
            prev_performance=performance;

            calculateflag = 1;
//        if(performance==0||Double.isInfinite(performance))
            if (Double.isInfinite(performance))
                return;

/*        if(app.phases[phasenum].performance==performance)
            return;*/

            app.phases[phasenum].performance = performance;
            app.performance=performance;
            for (int i = 0; i < 4; i++) {
//            Log.d("Monitor","core_: "+CoreManagement.core[i].CoreNumber);
                if (CoreManagement.core[i].app != null) {
//                Log.d("Monitor","core__: "+CoreManagement.core[i].CoreNumber);
                    if (CoreManagement.core[i].app.AppName.equals(app.AppName)) {
                    Log.d("Monitor","core___: "+CoreManagement.core[i].CoreNumber);
                        app = CoreManagement.core[i].app;
                        core = CoreManagement.core[i];
                        app.CalDeviation(performance);
                        if (app.phases[phasenum].AppType == Apptype.LATENCYAPP)
                            controlService(app, CoreManagement.core[i]);
                        if (app.phases[phasenum].AppType == Apptype.GAMEAPP)
                            controlGame(app, CoreManagement.core[i]);
                        if (app.phases[phasenum].AppType == Apptype.UIAPP)
                            controlUI(app, CoreManagement.core[i]);
                        if (app.phases[phasenum].AppType == Apptype.VIDEOAPP)
                            controlVideo(app, CoreManagement.core[i]);
					
/*					if(app.multiple_phase==1&&MainActivity.phasecontrol==1)
						app.phases[phasenum].freq=CoreManagement.core[i].Frequency;*/

                        break;
                    }
                }
            }
        }
    else
    {
        if (app.AppType == Apptype.GAMEAPP || app.AppType == Apptype.VIDEOAPP) {
            File file = new File(sdcard, "fps.txt");
            Log.d("Monitor", "reading fps");
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));

                while ((line = br.readLine()) == null) ;
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            File file = new File(sdcard, "response.txt");
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));

                while ((line = br.readLine()) == null) ;
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("Monitor", line);
        performance = Float.valueOf(line);
        if (Float.isNaN(performance))
            performance = 0;

        if(app.AppType==Apptype.UIAPP&&prev_performance==performance)
            return;
        prev_performance=performance;

        calculateflag = 1;
//        if(performance==0||Double.isInfinite(performance))
        if (Double.isInfinite(performance))
            return;

/*        if(app.phases[phasenum].performance==performance)
            return;*/

        app.performance = performance;
        for (int i = 0; i < 4; i++) {
//            Log.d("Monitor","core_: "+CoreManagement.core[i].CoreNumber);
            if (CoreManagement.core[i].app != null) {
//                Log.d("Monitor","core__: "+CoreManagement.core[i].CoreNumber);
                if (CoreManagement.core[i].app.AppName.equals(app.AppName)) {
//                    Log.d("Monitor","core___: "+CoreManagement.core[i].CoreNumber);
                    app = CoreManagement.core[i].app;
                    core = CoreManagement.core[i];
                    app.CalDeviation(performance);
                    if (app.AppType == Apptype.LATENCYAPP)
                        controlService(app, CoreManagement.core[i]);
                    if (app.AppType == Apptype.GAMEAPP)
                        controlGame(app, CoreManagement.core[i]);
                    if (app.AppType == Apptype.UIAPP)
                        controlUI(app, CoreManagement.core[i]);
                    if (app.AppType == Apptype.VIDEOAPP)
                        controlVideo(app, CoreManagement.core[i]);

/*					if(app.multiple_phase==1&&MainActivity.phasecontrol==1)
						app.phases[phasenum].freq=CoreManagement.core[i].Frequency;*/

                    break;
                }
            }
        }
    }
/*		Intent intent1=new Intent();
		intent1.setClassName("com.example.matrixapp1", "com.example.matrixapp1.MatrixCalculate");
		startService(intent1);*/

//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
        Log.d("Monitor", "Monitor is ending");
        return;
    }


    void controlVideo(AppEntity app, Core core)
    {
        int Freq = 0;
        float Dev=0;

        iterator=app.Deviation.iterator();
        while(iterator.hasNext())
            Dev=Dev+(Float) iterator.next();
        Dev=Dev/app.Deviation.size();
        Log.d("Monitor", "Deviation Video: "+Dev);
        if(MainActivity.energysaving==1)
        {
            if(Dev>0.1)
            {
                if(core.Frequency>app.PreferFreq)
                {
                    Freq=SystemSet.getLowerFreq(core.Frequency);
                    SystemSet.setCPUFreq(Freq, core.CoreNumber);
                    core.Frequency=Freq;
                }
            }
            else if(Dev<-0.15)
            {
                Freq=SystemSet.getHigherFreq(core.Frequency);
                SystemSet.setCPUFreq(Freq, core.CoreNumber);
                core.Frequency=Freq;
            }
        }
        Log.d("Monitor", "Frequency of core "+ core.CoreNumber+" "+core.Frequency);
//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*/


        try {
            Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            while((output=inputstream.readLine())!=null){
                break;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
        app.writefile.write(output.split("#"));
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
    }

    void controlUI(AppEntity app, Core core)
    {
        int Freq;
        float Dev=0;

        iterator=app.Deviation.iterator();
        while(iterator.hasNext())
            Dev=Dev+(Float) iterator.next();
        Dev=Dev/app.Deviation.size();
        Log.d("Monitor", "Deviation UI: "+Dev);

        if(MainActivity.energysaving==1)
        {
            if(Dev>0.1)
            {
                Freq=SystemSet.getHigherFreq(core.Frequency);
                SystemSet.setCPUFreq(Freq, core.CoreNumber);
                core.Frequency=Freq;
			
			/*if(app.multiple_phase!=1)
			{
			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}else
				app.phases[phasenum].freq=Freq;*/
            }
            else if(Dev<-0.1)
            {
                if(core.Frequency>app.PreferFreq)
                {
                    Freq=SystemSet.getLowerFreq(core.Frequency);
                    SystemSet.setCPUFreq(Freq, core.CoreNumber);
                    core.Frequency=Freq;
			/*if(app.multiple_phase!=1)
			{
			SystemSet.setCPUFreq(Freq, core.CoreNumber);
			core.Frequency=Freq;
			}else
				app.phases[phasenum].freq=Freq;*/
                }
            }//else
//			SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
        }

//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*/

        try {
            Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            while((output=inputstream.readLine())!=null){
                break;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
        app.writefile.write(output.split("#"));
        Log.d("Monitor", "Before Writing UI");

//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
    }

    void controlGame(AppEntity app, Core core)
    {
        int Freq;
        float Dev=0;

        iterator=app.Deviation.iterator();
        while(iterator.hasNext())
            Dev=Dev+(Float) iterator.next();
        Dev=Dev/app.Deviation.size();

        Log.d("Monitor","Deviation Size: "+app.Deviation.size());

        if(app.Deviation.size()==0)
            return;

        Log.d("Monitor", "Deviation Game: "+Dev);

        Log.d("Monitor", "Core Freq: "+core.Frequency);
        if(MainActivity.energysaving==1)
        {
            if(Dev>0.2)
            {
                if(core.Frequency>app.PreferFreq)
                {
                    Freq=SystemSet.getLowerFreq(core.Frequency);
                    SystemSet.setCPUFreq(Freq, core.CoreNumber);
                    core.Frequency=Freq;
                }
            }
            else if(Dev<-0.15)
            {
                Freq=SystemSet.getHigherFreq(core.Frequency);
      			SystemSet.setCPUFreq(Freq, core.CoreNumber);
                core.Frequency=Freq;
            }//else
//			SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
        }

//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*/


        try {
            Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            while((output=inputstream.readLine())!=null){
                break;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d("Monitor","Exception");
        }
        output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
        Log.d("Monitor","Before Writing Game");
        app.writefile.write(output.split("#"));
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
    }

    void controlService(AppEntity app, Core core)
    {
        int Freq;
        float Dev=0;

        iterator=app.Deviation.iterator();
        while(iterator.hasNext())
            Dev=Dev+(Float) iterator.next();
        Dev=Dev/app.Deviation.size();
        Log.d("Core Frequency", Integer.toString(core.Frequency));
        Log.d("Deviation Service: ", Float.toString(Dev));
        if(MainActivity.energysaving==1)
        {
            if(Dev>0.1)
            {
                Freq=SystemSet.getHigherFreq(core.Frequency);
                Log.d("Monitor", "Before setting freq of cpu"+core.CoreNumber);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
                Log.d("Monitor", "After setting freq of cpu"+core.CoreNumber);
                core.Frequency=Freq;
            }
            else if(Dev<-0.1)
            {
                if(core.Frequency>app.PreferFreq)
                {
                    Freq=SystemSet.getLowerFreq(core.Frequency);
//			SystemSet.setCPUFreq(Freq, core.CoreNumber);
                    core.Frequency=Freq;
                }
            }
        }

//		app.writefile.write((core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev).split("#"));
/*		Thread fileThread=new Thread(new writeThread(core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev));
		fileThread.start();*/

        try {
            Process process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
            BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

            while((output=inputstream.readLine())!=null){
                break;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d("Monitor", "2");
        output=core.CoreNumber+"#"+core.Frequency+"#"+app.sweetSpotPerformance+"#"+app.performance+"#"+Dev+"#"+output+"#"+" "+"#"+System.currentTimeMillis();
        app.writefile.write(output.split("#"));
//		SystemSet.setCPUFreq(core.Frequency, core.CoreNumber);
    }

	
/*	public class writeThread implements Runnable{

		String content;
		
		writeThread(String content)
		{
			this.content=content;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			DataOutputStream outputstream=new DataOutputStream(SystemSet.suCPU.getOutputStream());
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(SystemSet.suCPU.getInputStream()));
			String output = null;
			
			try {
				outputstream.writeBytes("busybox cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_max_freq\n");
				outputstream.flush();
				
				while((output=inputstream.readLine())!=null){
	    			break;
	    		}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			output=content+"#"+output;
			app.writefile.write(output.split("#"));
		}
		
	}*/
}
