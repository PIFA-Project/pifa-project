package com.vickie.anroidem2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SystemSet {

	public static native int setCPUAffinity(int mypid, int cpunum);
	
	static Process suCPU;
	
	static {
	    System.loadLibrary("ndk_cpuaffinity");
	}
	
	@SuppressLint("NewApi")
/*	public static int setcpuAffinity(int mypid, int cpunum)
	{
		int pid;
		pid=setCPUAffinity(mypid, cpunum);
		return pid;
	}
	*/
	
	public static void setCPUFreq(int Freq, int CPU)
	{
		Log.d("setCPUFreq", Freq+", "+CPU);
		try {
			DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());

/*
			outputstream.writeBytes("echo 300000 > /sys/devices/system/cpu/cpu"+CPU+"/cpufreq/scaling_min_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+CPU+"/cpufreq/scaling_max_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+CPU+"/cpufreq/scaling_min_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo 300000 > /sys/devices/system/cpu/cpu"+(CPU+1)+"/cpufreq/scaling_min_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+(CPU+1)+"/cpufreq/scaling_max_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+(CPU+1)+"/cpufreq/scaling_min_freq\n");
			outputstream.flush();
*/

/*
			outputstream.writeBytes("echo 300000 > /sys/devices/system/cpu/cpu"+(CPU+2)+"/cpufreq/scaling_min_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+(CPU+2)+"/cpufreq/scaling_max_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+(CPU+2)+"/cpufreq/scaling_min_freq\n");
			outputstream.flush();
*/

			outputstream.writeBytes("echo "+Freq +"> /sys/devices/system/cpu/cpu"+CPU+"/cpufreq/scaling_max_freq\n");
			outputstream.flush();

			/*outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu"+(CPU+1)+"/cpufreq/scaling_max_freq\n");
			outputstream.flush();*/

/*			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq\n");
			outputstream.flush();

			outputstream.writeBytes("echo "+Freq+" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq\n");
			outputstream.flush();*/

/*			outputstream.writeBytes("exit\n");
			outputstream.flush();*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void shutoffCore(Core core)
	{
		try {
    		suCPU=Runtime.getRuntime().exec("su");
    		DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());
    		
    		outputstream.writeBytes("stop mpdecision\n");
        	outputstream.flush();
        	
        	outputstream.writeBytes("chmod 664 /sys/devices/system/cpu/cpu"+core.CoreNumber+"/online\n");
        	outputstream.flush();
        	
        	outputstream.writeBytes("echo 0 > /sys/devices/system/cpu/cpu"+core.CoreNumber+"/online\n");
        	outputstream.flush();
        	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public static void setGovernor(String governor)
	{
    	try {
    		suCPU=Runtime.getRuntime().exec("su");
    		DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());
    		
    		outputstream.writeBytes("echo "+governor+" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor\n");
        	outputstream.flush();
        	
        	outputstream.writeBytes("echo "+governor+" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor\n");
        	outputstream.flush();
        	
        	outputstream.writeBytes("echo "+governor+" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor\n");
        	outputstream.flush();
        	
        	outputstream.writeBytes("echo "+governor+" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor\n");
        	outputstream.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	@SuppressLint("InlinedApi")
	public static int runApp(AppEntity app, Core core, Context context)
	{
		int pid=-1;
		
		/*if(app.Apptype==Apptype.VIDEOAPP)
		{
			Process su;
			try {
				su = Runtime.getRuntime().exec("su");
				DataOutputStream outputstream=new DataOutputStream(su.getOutputStream());
	    		
	    		outputstream.writeBytes("am instrument "+app.AppVideoTest+"/android.test.InstrumentationTestRunner\n");
	    		Log.d("SystemSet", "am instrumentation "+app.AppVideoTest+"/android.test.InstrumentationTestRunner\n");
	    		outputstream.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			core.Frequency=app.PreferFreq;
			SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
			Log.d("SystemSet", "App"+app.AppName+"is starting");
		}*/
		if(app.AppType==Apptype.LATENCYAPP)
		{
			/*Intent intent = new Intent();
			intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
			intent.setAction("com.example.androidem");
			intent.putExtra("StartApp", app.AppName);
			context.sendBroadcast(intent);*/
			
			try {
				DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());
	    		
	    		outputstream.writeBytes("am startservice --ei CPU "+Integer.toString(core.CoreNumber)+" "+app.AppName+"/."+app.ServiceName+"\n");
//	    		Log.d("SystemSet", "am instrumentation "+app.AppVideoTest+"/android.test.InstrumentationTestRunner\n");
	    		outputstream.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			core.Frequency=app.PreferFreq;
			SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
			Log.d("SystemSet", "App"+app.AppName+"is starting");
		}
		else if(app.AppType==Apptype.GAMEAPP||app.AppType==Apptype.UIAPP||app.AppType==Apptype.VIDEOAPP)
		{
			Log.d("SystemSet", "Starting... "+app.AppName+" "+app.AppVideoTest);
			
			if(app.AppVideoTest==null)
			{
			Intent intent = context.getPackageManager().getLaunchIntentForPackage(app.AppName);
			if (intent!=null)
				context.startActivity(intent);
			}else
				context.startInstrumentation(new ComponentName(app.AppVideoTest, "android.test.InstrumentationTestRunner"), null, null);
			
			try {
				DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
	    		outputstream.writeBytes("am startservice --ei CPU "+Integer.toString(core.CoreNumber)+" "+app.AppName+"/."+app.ServiceName+"\n");
//	    		Log.d("SystemSet", "am instrumentation "+app.AppVideoTest+"/android.test.InstrumentationTestRunner\n");
	    		Log.d("SystemSet", "start CPU affinity service");
	    		outputstream.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			core.Frequency=app.PreferFreq;
			Log.d("set frequency", app.AppName+", "+app.PreferFreq+", "+core.Frequency+", "+core.CoreNumber);
			SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
		}
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ActivityManager mgr = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
	    List<RunningAppProcessInfo> processes = mgr.getRunningAppProcesses();
	    
	    if(processes==null)
	    	Log.d("SystemSet", "processes is null");
/*	    while(pid==-1)
	    {*/
	    
/*	    for(Iterator<RunningAppProcessInfo> i=processes.iterator(); i.hasNext();)
	    {
	    	RunningAppProcessInfo proc=(RunningAppProcessInfo)i.next();
//	    	Log.d("SystemSet", "processName: "+proc.processName);
	    	if(proc.processName.equalsIgnoreCase(app.AppName)){
	              pid = proc.pid;
	              Log.d("SystemSet", "The PID of "+app.AppName+" is "+pid);
	           }
	    }
//	    }
		app.PID=pid;*/

		try {
			Process process= Runtime.getRuntime().exec("su");
			DataOutputStream outputstream=new DataOutputStream(process.getOutputStream());
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			outputstream.writeBytes("ps | grep " + app.AppName + " \n");

//            Log.d("SystemSet", "outstream");
//			inputstream.reset();
			String string=inputstream.readLine();
//			Log.d("SystemSet", "outstream: "+string);
			if (string!=null) {
				String[] string_array = string.split(" ");
				for (int i = 0; i < string_array.length; i++) {
//					Log.d("SystemSet", string_array[i]);
					if(string_array[i].equals("grep")) {
						if(inputstream.ready()==true)
							string=inputstream.readLine();
						else
							string=null;
						break;
					}
				}
			}
			if (string==null)
				;
			else {
//				Log.d("SystemSet","Output String:"+string);
				outputstream.flush();
				String[] string_array = string.split(" ");

/*				for (int i = 0; i < string_array.length; i++)
					Log.d("SystemSet", string.split(" ")[i]);*/

				if(string_array[0].equals(""))
					pid=Integer.parseInt(string_array[2]);
				else {
					for(int i=1;i<5;i++)
						if(!string_array[i].equals("")) {
							pid = Integer.parseInt(string_array[i]);
							break;
						}
				}
			}
			inputstream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		app.PID=pid;
		Log.d("SystemSet", "The PID of " + app.AppName + " is " + pid);


//		if(MainActivity.energysaving==1)
//		{
//	    if(app.multiple_phase==1)
//		{
	    	Log.d("SystemSet", "start phasecontroler "+app.trainingDataPath); //need revise
			app.phasecontroler=new PhaseControlClassifier(app, core, context, app.trainingDataPath);
			app.timer=new Timer();
			app.timer.schedule(app.phasecontroler, 0, 2000);
//		}
//		}
	    
		return pid;
	}
	
	public static void stopApp(AppEntity app, Context context)
	{
		try {
			DataOutputStream outputstream=new DataOutputStream(suCPU.getOutputStream());
    		
    		outputstream.writeBytes("am force-stop "+app.AppName+"\n");
    		outputstream.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int getHigherFreq(int Freq)
	{
		/*if(Freq==2265600)
			return 2265600;
		else if(Freq==1958400)
			return 2265600;
		else if(Freq==1728000)
			return 1958400;
		else */if(Freq==1574400)
//			return 1728000;
			return 1574400;
		else if(Freq==1497600)
			return 1574400;
		else if(Freq==1267200)
			return 1497600;
		else if(Freq==1190400)
			return 1267200;
		else if(Freq==1036800)
			return 1190400;
		else if(Freq==960000)
			return 1036800;
		else if(Freq==883200)
			return 960000;
		else if(Freq==729600)
			return 883200;
		else if(Freq==652800)
			return 729600;
		else if(Freq==422400)
			return 652800;
		else if(Freq==300000)
			return 422400;
		else
			return 0;
	}
	
	public static int getLowerFreq(int Freq)
	{/*
		if(Freq==2265600)
			return 1958400;
		else if(Freq==1958400)
			return 1728000;
		else if(Freq==1728000)
			return 1574400;
		else */if(Freq==1574400)
			return 1497600;
		else if(Freq==1497600)
			return 1267200;
		else if(Freq==1267200)
			return 1190400;
		else if(Freq==1190400)
			return 1036800;
		else if(Freq==1036800)
			return 960000;
		else if(Freq==960000)
			return 883200;
		else if(Freq==883200)
			return 729600;
		else if(Freq==729600)
			return 652800;
		else if(Freq==652800)
			return 422400;
		else if(Freq==422400)
			return 300000;
		else if(Freq==300000)
			return 300000;
		else
			return 0;
	}
}
