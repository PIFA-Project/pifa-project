package com.vickie.anroidem2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.BayesNet;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.AllFilter;

public class PhaseControlClassifier extends TimerTask{

	WriteFile writefile;
	String appname;
	Timer timer=null, timerwrite;
	TimerTask timertask=null, timerwritetask;
	double cpuUse, gpuUse, memUse;
	long cpuTotalBefore=0;
	long cpuPidBefore=0;
	int Freq;
	Queue<String> file;
	Queue<Double> cpuUsageQ, gpuUsageQ, memUsageQ;
	Phase curPhase;
	AppEntity app;
	Core core;
	Context context;
	int changecounter=0;
	
	int timeCounter=0;
	int timeCounter1=0;
	int cur_phase_num=0;
	
	Classifier model;
	Instances OriginalTrain; 
	AllFilter m_filter= new AllFilter();
	public String trainingDataPath;
	Monitor1 monitor;
	int monitor_count=0;
	
	public PhaseControlClassifier(AppEntity app, Core core, Context context, String trainingDataPath)
	{
		this.app=app;
		this.core=core;
		this.context=context;
		if(trainingDataPath==null)
			this.trainingDataPath=" ";
		else
			this.trainingDataPath=trainingDataPath;
		
		Log.d("PhaseControl", "In the instructor");
		cpuUsageQ=new LinkedBlockingQueue<Double>();
		gpuUsageQ=new LinkedBlockingQueue<Double>();
		memUsageQ=new LinkedBlockingQueue<Double>();
		file=new LinkedBlockingQueue<String>();
		monitor=new Monitor1();
		writefile=new WriteFile(app.AppName+"_phasedata.csv");
//		Log.d("PhaseControl",trainingDataPath);

		try {
			DataSource source = new DataSource(context.getAssets().open(trainingDataPath+".arff"));
			OriginalTrain=source.getDataSet();
			OriginalTrain.setClassIndex(OriginalTrain.numAttributes() - 1);
			m_filter.setInputFormat(OriginalTrain);
			model=new BayesNet();
			model.buildClassifier(OriginalTrain);
//			model = (BayesNet) weka.core.SerializationHelper.read(context.getAssets().open("luckyfishing_mold.model"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
//		Log.d("PhaseControl", "phase control")

		File sdcard = Environment.getExternalStorageDirectory();
		File file1 = new File(sdcard, "test.txt");
		String line;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file1));

			while ((line = br.readLine()) == null) Log.d("Monitor","line is none");
//			Log.d("Monitor",line);
			br.close();
			if(line.equals("no"))
			{
				Intent intent2=new Intent();
				intent2.setClassName("com.vickie.anroidem2", "com.vickie.anroidem2.StartScheduler");
				intent2.putExtra("AppName", app.AppName);
				context.startService(intent2);

				FileOutputStream fileout=new FileOutputStream(file1);
				OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
				outputWriter.write("yes");
				Log.d("PhaseControlClassifier","after writing yes");
				outputWriter.close();

				FileOutputStream response=new FileOutputStream(new File(sdcard,"response.txt"));
				OutputStreamWriter outputWriter1=new OutputStreamWriter(response);
				outputWriter1.write("0");
				Log.d("PhaseControlClassifier", "after writing 0");
				outputWriter1.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(MainActivity.energysaving==1&&app.multiple_phase==1)
		{
			readPhase(cpuUse, gpuUse, memUse);
			timeCounter++;
			timeCounter1++;

			detectPhase1(cpuUse, gpuUse, memUse);

			Log.d("setCPUAffinity", "Phase 0 Freq 2*: " + app.phases[0].freq);
			if (timeCounter1 == 1) {
				int i = 10;
				timeCounter1 = 0;

				i = identifyPhase(cpuUse, gpuUse, memUse, app.phases);

				Log.d("PhaseControl", "The phase is: " + i);


//		file.add("break i: "+i);
				if (i < app.phases.length) {
					if (curPhase == null) {
//			Log.d("PhaseControl", "Loading Phase");
			
			/*remove when running dummy app*/
						Log.d("setCPUFreq","Phase Number: "+i);
						Log.d("setCPUFreq","Phase Type: "+ app.phases[i].AppType);
						Log.d("setCPUFreq","Phase Freq: "+ app.phases[i].freq);
						Log.d("setCPUFreq","Phase 0 Freq: "+app.phases[0].freq);
						Log.d("setCPUFreq","App Name: "+app.AppName);
						loadPhase(app.phases[i]);
						cur_phase_num = i;

//			writefile.write("Setting Freq".split("#"));
						for (int j = 0; j < app.phases.length; j++)
							app.phases[j].count = 0;
						app.phases[i].count++;
					} else if (!curPhase.equals(app.phases[i])) {
						changecounter++;
						if (changecounter > 1) {
					
					/*remove when running dummy app*/
							Log.d("setCPUFreq","Phase Number: "+i);
							Log.d("setCPUFreq","Phase type: "+ app.phases[i].AppType);
							Log.d("setCPUFreq","Phase Freq: "+ app.phases[i].freq);
							Log.d("setCPUFreq","Phase 0 Freq: "+app.phases[0].freq);
							Log.d("setCPUFreq","App Name: "+app.AppName);
							loadPhase(app.phases[i]);
							app.Deviation.clear();
							cur_phase_num = i;

							file.add("Setting Freq#" + i);
						}
						for (int j = 0; j < app.phases.length; j++)
							app.phases[j].count = 0;
						app.phases[i].count++;
					} else {
						changecounter = 0;
					}
				}
			}

			Log.d("PhaseContro", "Current phase: "+cur_phase_num);

			if (timeCounter == 50) {
				timeCounter = 0;

				while (!file.isEmpty()) {
					String string = file.remove();
					Log.d("PhaseControl", string);
					writefile.write(string.split("#"));
				}
			}
			monitor_count++;
			if(monitor_count>2) {
				try {
					Process process=Runtime.getRuntime().exec("su");
					DataOutputStream outputstream=new DataOutputStream(process.getOutputStream());
					outputstream.writeBytes("/data/user/0/com.hipipal.qpyplus/files/bin/qpython-android5.sh /sdcard/com.hipipal.qpyplus/scripts/systrace3/systrace.py\n");
					outputstream.flush();
					outputstream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				monitor.onStartCommand(app, cur_phase_num, context);
				Log.d("setCPUAffinity", "Phase 0 Freq 3*: " + app.phases[0].freq);
				monitor_count=0;
			}
		}else {
			monitor_count++;
			if (monitor_count > 2) {
				try {
					Process process = Runtime.getRuntime().exec("su");
					DataOutputStream outputstream = new DataOutputStream(process.getOutputStream());
					outputstream.writeBytes("/data/user/0/com.hipipal.qpyplus/files/bin/qpython-android5.sh /sdcard/com.hipipal.qpyplus/scripts/systrace3/systrace.py\n");
					Log.d("PhaseControl","when no");
					outputstream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				monitor.onStartCommand(app, -1, context);
				monitor_count = 0;
			}
		}
	}
	
	Instance BuildInstance(double cpuUse, double gpuUse, double memUse, Instances OriginalTrain)
	{
		Instance inst=new Instance(4);
		
		Attribute messageAtt1 = OriginalTrain.attribute("cpuUsage");
		inst.setValue(messageAtt1, cpuUse); 
		Attribute messageAtt2 = OriginalTrain.attribute("gpuUsage");
		inst.setValue(messageAtt2, gpuUse); 
		Attribute messageAtt3 = OriginalTrain.attribute("memUsage");
		inst.setValue(messageAtt3, memUse);
		
		inst.setDataset(OriginalTrain);
		
		return inst;
	}
	
	public void loadPhase(Phase phase)
	{
		curPhase=phase;
		app.AppType=phase.AppType;

		Log.d("PhaseControl", "Loading Phase");
		if(MainActivity.phasecontrol==1)
		{
			app.PreferFreq=phase.freq;
			app.sweetSpotPerformance=phase.sweetperformance;
		}
		app.Deviation.clear();


		Log.d("setCPUFreq",": "+phase.freq+": "+app.PreferFreq);
		SystemSet.setCPUFreq(app.PreferFreq, core.CoreNumber);
		core.Frequency=app.PreferFreq;
	}

	public void readPhase(double cpuUsage, double gpuUsage, double memUsage)
	{
		gpuUse=gpuUsage();
		cpuUse=cpuUsage_App();
		memUse=memUsage_App();
		
		Process process;
		String result = null;
		try {
			process = Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu"+core.CoreNumber+"/cpufreq/scaling_cur_freq");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			result=inputstream.readLine();
			while(result==null||result.equals(""))
				result=inputstream.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d("PhaseControlClassifier","readPhase: "+cpuUse+", "+gpuUse+", "+memUse);
		file.add(cpuUse+"#"+gpuUse+"#"+memUse+"#"+result+"#"+core.CoreNumber+"#"+SystemClock.uptimeMillis()+"#"+changecounter);
		
		cpuUsageQ.add(cpuUsage);
		gpuUsageQ.add(gpuUsage);
		memUsageQ.add(memUsage);
	}
	
	public void detectPhase1(double cpuUsage, double gpuUsage, double memUsage)
	{
		cpuUsage=0;
		gpuUsage=0;
		memUsage=0;
		int size_cpu=cpuUsageQ.size();
		int size_gpu=gpuUsageQ.size();
		int size_mem=memUsageQ.size();
		
		while(!cpuUsageQ.isEmpty())
			cpuUsage+=cpuUsageQ.remove();
		cpuUsage=cpuUsage/size_cpu;
		
		while(!gpuUsageQ.isEmpty())
			gpuUsage+=gpuUsageQ.remove();
		gpuUsage=gpuUsage/size_gpu;
		
		while(!memUsageQ.isEmpty())
			memUsage+=memUsageQ.remove();
		memUsage=memUsage/size_mem;
	}
	
	public int identifyPhase(double cpuUsage, double gpuUsage, double memUsage, Phase[] phase)
	{
		double[] distance=new double[phase.length];
		double predicted = 0;
		
		Instance instance=BuildInstance(cpuUse, gpuUse, memUse, OriginalTrain);
		
		Log.d("PhaseControlClassifier", instance.toString());
		m_filter.input(instance);
		Instance filteredInstance=m_filter.output();
//		Log.d("PhaseControlClassifier", filteredInstance.toString());
		try {
			predicted = model.classifyInstance(filteredInstance);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Log.d("PhaseControlClassifier", Double.toString(predicted));
		Log.d("PhaseControlClassifier", OriginalTrain.classAttribute().value((int)predicted));
		
		if(OriginalTrain.classAttribute().value((int)predicted).equals("p0"))
			return 0;
		else if(OriginalTrain.classAttribute().value((int)predicted).equals("p1"))
			return 1;
		else if(OriginalTrain.classAttribute().value((int)predicted).equals("p2"))
			return 2;
		else if(OriginalTrain.classAttribute().value((int)predicted).equals("p3"))
			return 3;
		else
			return 4;
	}
	
	public Double cpuUsage_App()
	{
		long cpuTotalAfter;
		long cpuPidAfter;
		
		double usage;
		
		if(cpuTotalBefore==0||cpuPidBefore==0)
		{
			cpuTotalBefore=getCPUTotal();
			cpuPidBefore=getCPUPid();
			return 0.00;
		}
		
		cpuTotalAfter=getCPUTotal();
		cpuPidAfter=getCPUPid();
		
		usage=(double)(cpuPidAfter-cpuPidBefore)/(cpuTotalAfter-cpuTotalBefore);
		cpuTotalBefore=cpuTotalAfter;
		cpuPidBefore=cpuPidAfter;
		
		return usage;
	}

	public long getCPUTotal()
	{
		long cpuTotal=0;
		String result;
		String[] returnString;

		try {
			Process process = Runtime.getRuntime().exec("cat /proc/stat");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

			result=inputstream.readLine();
			inputstream.close();
			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			returnString=result.split(" ");
			for(int i=1; i<returnString.length; i++)
			{
				if(!returnString[i].equals(""))
					cpuTotal+= Long.valueOf(returnString[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cpuTotal;
	}

	public long getCPUPid()
	{
		long cpuPid = 0;

		String result;
		String[] returnString;

		try {
			Process process = Runtime.getRuntime().exec("cat /proc/"+app.PID+"/stat");
//			Log.d("PhaseControler", Integer.toString(app.PID));
			if(process==null)
				return 0;
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

			result=inputstream.readLine();
			if(result==null)
			{
				inputstream.close();
				return 0;
			}
			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			inputstream.close();
			returnString=result.split(" ");
			cpuPid= Long.valueOf(returnString[13])+ Long.valueOf(returnString[14]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cpuPid;
	}

	public double gpuUsage()
	{
		String result = null;
		String[] returnString, gpudata;
		double usage=0;
		gpudata=new String[2];
		Process process;

		try {
			process = Runtime.getRuntime().exec("cat /sys/class/kgsl/kgsl-3d0/gpubusy");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

			result=inputstream.readLine();

			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			inputstream.close();
//			Log.d("PhaseService", result);
			returnString=result.split(" ");
			int j=0;
			for(int i=0; i<returnString.length; i++)
			{
				if(!returnString[i].equals(""))
					gpudata[j++]=returnString[i];
			}
//			Log.d("PhaseService", gpudata[0]+", "+gpudata[1]);
			if(Integer.valueOf(gpudata[1])!=0)
				usage= Double.valueOf(gpudata[0])/ Integer.valueOf(gpudata[1]);
			else
				usage=0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return usage;
	}

	@SuppressLint("NewApi")
	public double memUsage_App()
	{
		double usedmem = 0;
		double totalmem=0;
		String result;
		String[] result_arr;
/*		ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		MemoryInfo memoryInfo=new MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);*/

		Process process;
		try {
			process = Runtime.getRuntime().exec("cat /proc/"+app.PID+"/stat");
			BufferedReader inputstream=new BufferedReader(new InputStreamReader(process.getInputStream()));

			result=inputstream.readLine();

			if(result==null)
			{
				inputstream.close();
				return 0;
			}

			while(result.equals(""))
			{
				result=inputstream.readLine();
			}
			inputstream.close();
			usedmem= Double.valueOf(result.split(" ")[23]);

			process = Runtime.getRuntime().exec("cat /proc/meminfo");
			BufferedReader inputstream1=new BufferedReader(new InputStreamReader(process.getInputStream()));

			result=inputstream1.readLine();

			while(result.equals(""))
			{
				result=inputstream1.readLine();
			}
			inputstream1.close();
			result_arr=result.split(" ");
			for(int i=1;i<result_arr.length;i++)
			{
				if(!result_arr[i].equals(""))
				{
					totalmem= Double.valueOf(result_arr[i]);
					break;
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		usage=(usage*4)/memoryInfo.totalMem;
		return 4*usedmem/totalmem;
	}
}
