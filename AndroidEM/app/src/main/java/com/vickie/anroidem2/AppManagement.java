package com.vickie.anroidem2;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;

public class AppManagement {
	public static Queue<AppEntity> Queue1;
	public static Queue<AppEntity> Queue2;
	public static Queue<AppEntity> BusyService;
	public static int finishedapp;
	
	public static void InitAppManagement()
	{
		Queue1=new LinkedBlockingQueue<AppEntity>();
		Queue2=new LinkedBlockingQueue<AppEntity>();
		BusyService=new LinkedBlockingQueue<AppEntity>();
		finishedapp=0;

		AppEntity App1;
		AppEntity App2;
		AppEntity App3;
		AppEntity App4;
		AppEntity App5;
		AppEntity App6;
		AppEntity App7;
		AppEntity App8;
		AppEntity App9;
		AppEntity App10;
		AppEntity App11;
		AppEntity App15;
		AppEntity App16;
		
		AppEntity App12;
		AppEntity App13;
		AppEntity App14;
		AppEntity App17;
		AppEntity App18;
		AppEntity App19;
		AppEntity App20;

		AppEntity App21;
		AppEntity App22;
		AppEntity App23;
		AppEntity App24;
		AppEntity App25;
		AppEntity App26;
		AppEntity App27;
		AppEntity App28;
		AppEntity App29;
		AppEntity App30;
		
		if(MainActivity.energysaving==1)
		{
			App1=new AppEntity("pt.isec.tp.am", "pt.isec.tp.am.test", Apptype.GAMEAPP, 1036800, "CPUAffinityService", 30);
			App2=new AppEntity("com.replica.replicaisland", "com.replica.replicaisland.test", Apptype.GAMEAPP, 652800, "CPUAffinityService", 60);
			App3=new AppEntity("com.example.matrixapp1", Apptype.LATENCYAPP, 652800, "MatrixCalculate", 2000, 39900, 39900);
			App4=new AppEntity("com.example.matrixapp2", Apptype.LATENCYAPP, 960000, "MatrixCalculate", 1150, 20000, 20000);
			App5=new AppEntity("com.example.matrixapp3", Apptype.LATENCYAPP, 960000, "MatrixCalculate", 1083, 12000, 12000);
			App6=new AppEntity("com.example.matrixapp4", Apptype.LATENCYAPP, 1036800, "MatrixCalculate", 1033, 27000, 27000);
			App7=new AppEntity("com.example.matrixapp5", Apptype.LATENCYAPP, 422400, "MatrixCalculate", 2820, 22400, 22400);
			App8=new AppEntity("org.jtb.alogcat", "com.vickie.alogcattest", Apptype.UIAPP, 960000, "CPUAffinityService", 100);
			App9=new AppEntity("si.modrajagoda.didi", "com.vickie.diditest", Apptype.UIAPP, 1036800, "CPUAffinityService", 100);
			App10=new AppEntity("com.example.openglvideo", "com.example.openglvideo.test", Apptype.VIDEOAPP, 300000, "CPUAffinityService", 24);
			App11=new AppEntity("com.vickie.videojohnoliver", "com.vickie.videojohnoliver.test", Apptype.VIDEOAPP, 300000, "CPUAffinityService", 24);
			App15=new AppEntity("com.aemobile.games.luckyfishing", "com.vickie.luckyfishingtest", Apptype.GAMEAPP, 300000, "CPUAffinityService", 16);
			App16=new AppEntity("com.davemorrissey.labs.subscaleview.sample", "com.vickie.subscaleviewtest", Apptype.UIAPP, 883200, "CPUAffinityService", 100);
			App17=new AppEntity("com.vickie.dummyapp", Apptype.UIAPP, 300000, "dummyService", 10000, 10000, 10000);
			App18=new AppEntity("com.vickie.myapp1", "com.vickie.myapp1.test", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App19=new AppEntity("com.qrbarcodescanner", "com.qrbarcodescanner.test", Apptype.UIAPP, 729600, "CPUAffinityService", 100);
			App20=new AppEntity("com.rarlab.rar", "com.rarlab.rar.test", Apptype.UIAPP, 300000, "CPUAffinityService", 100);
			App21=new AppEntity("com.king.candycrushsaga","com.king.candycrushsaga.test",Apptype.GAMEAPP,1036800,"CPUAffinityService",60);
			App22=new AppEntity("com.rovio.angrybirds","com.rovio.angrybirds.test",Apptype.GAMEAPP,1036800,"CPUAffinityService",60);
			App23=new AppEntity("com.kmplayer","com.kmplayer.test",Apptype.VIDEOAPP,300000,"CPUAffinityService",30);
			App24=new AppEntity("com.asus.deskclock","com.asus.deskclock.test",Apptype.UIAPP,729600,"CPUAffinityService",100);
			App25=new AppEntity("com.rarlab.rar","com.rarlab.rar.test",Apptype.UIAPP,300000,"CPUAffinityService",100);
			App26=new AppEntity("org.coolreader","org.coolreader.test",Apptype.UIAPP,300000,"CPUAffinityService",100);
			App27=new AppEntity("edu.umich.PowerTutor","edu.umich.PowerTutor.test",Apptype.UIAPP,300000,"CPUAffinityService",100);
			App28=new AppEntity("com.pixatel.apps.notepad","com.pixatel.apps.notepad.test",Apptype.UIAPP,300000,"CPUAffinityService",100);
			App29=new AppEntity("com.tatkovlab.sdcardcleaner","com.tatkovlab.sdcardcleaner.test",Apptype.UIAPP,422400,"CPUAffinityService",100);
			App30=new AppEntity("com.cmsecurity.lite","com.cmsecurity.lite.test",Apptype.UIAPP,729600,"CPUAffinityService",100);
			Log.d("AppManagement", "after init apps");
			init_Phase(App1);
			init_Phase(App2);
			init_Phase(App11);
			init_Phase(App15);
			init_Phase(App16);
			init_Phase(App17);
			init_Phase(App18);
			init_Phase(App19);
			init_Phase(App21);
			init_Phase(App22);
			init_Phase(App23);
			init_Phase(App29);
			init_Phase(App30);
//			Log.d("setCPUFreq","Phase Freq: "+App22.phases[0].freq);
			Log.d("AppManagement", "after init phase");
		}
		else
		{
			App1=new AppEntity("pt.isec.tp.am", "pt.isec.tp.am.test", Apptype.GAMEAPP, 1958400, "CPUAffinityService", 30);
			App2=new AppEntity("com.replica.replicaisland", "com.replica.replicaisland.test", Apptype.GAMEAPP, 2265600, "CPUAffinityService", 17.2);
			App3=new AppEntity("com.example.matrixapp1", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 2000, 39900, 39900);
			App4=new AppEntity("com.example.matrixapp2", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 20000/15, 20000, 20000);
			App5=new AppEntity("com.example.matrixapp3", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 12000/10, 12000, 12000);
			App6=new AppEntity("com.example.matrixapp4", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 27000/8, 27000, 27000);
			App7=new AppEntity("com.example.matrixapp5", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 22400/12, 22400, 22400);
			App8=new AppEntity("org.jtb.alogcat", "com.vickie.alogcattest", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App9=new AppEntity("si.modrajagoda.didi", "com.vickie.diditest", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App10=new AppEntity("com.example.openglvideo", "com.vickie.openglvideotest", Apptype.VIDEOAPP, 1958400, "CPUAffinityService",24);
			App11=new AppEntity("com.vickie.videojohnoliver", "com.vickie.videojohnoliver.test", Apptype.VIDEOAPP, 1958400, "CPUAffinityService", 24);
			App15=new AppEntity("com.aemobile.games.luckyfishing", "com.vickie.luckyfishingtest", Apptype.GAMEAPP, 1958400, "CPUAffinityService", 16);
			App16=new AppEntity("com.davemorrissey.labs.subscaleview.sample", "com.vickie.subscaleviewtest", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App17=new AppEntity("com.vickie.dummyapp", Apptype.UIAPP, 1958400, "dummyService", 10000, 10000, 10000);
			App18=new AppEntity("com.vickie.myapp1", "com.vickie.myapp1.test", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App19=new AppEntity("com.qrbarcodescanner", "com.qrbarcodescanner.test", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App20=new AppEntity("com.rarlab.rar", Apptype.UIAPP, 1958400, "CPUAffinityService", 100, 100, 100);
			App21=new AppEntity("com.king.candycrushsaga","com.king.candycrushsaga.test",Apptype.GAMEAPP,1958400,"CPUAffinityService",60);
			App22=new AppEntity("com.rovio.angrybirds","com.rovio.angrybirds.test",Apptype.GAMEAPP,1958400,"CPUAffinityService",60);
			App23=new AppEntity("com.kmplayer","com.kmplayer.test",Apptype.VIDEOAPP,1958400,"CPUAffinityService",24);
			App24=new AppEntity("com.asus.deskclock","com.asus.deskclock.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
			App25=new AppEntity("com.rarlab.rar","com.rarlab.rar.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
			App26=new AppEntity("org.coolreader","org.coolreader.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
			App27=new AppEntity("edu.umich.PowerTutor","edu.umich.PowerTutor.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
			App28=new AppEntity("com.pixatel.apps.notepad","com.pixatel.apps.notepad.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
			App29=new AppEntity("com.tatkovlab.sdcardcleaner","com.tatkovlab.sdcardcleaner.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
			App30=new AppEntity("com.cmsecurity.lite","com.cmsecurity.lite.test",Apptype.UIAPP,1958400,"CPUAffinityService",100);
		}
		
		if(MainActivity.phasecontrol==1||MainActivity.phasecontrol==2)
		{
			App1.multiple_phase=1;
			App11.multiple_phase=1;
			App2.multiple_phase=1;
			App15.multiple_phase=1;
			App17.multiple_phase=1;
			App18.multiple_phase=1;
			App19.multiple_phase=1;
			App16.multiple_phase=1;
			App21.multiple_phase=1;
			App22.multiple_phase=1;
			App23.multiple_phase=1;
			App29.multiple_phase=1;
			App30.multiple_phase=1;
		}
		
		App12=new AppEntity("com.example.busyservice1", Apptype.LATENCYAPP, 1497600, "MatrixCalculate", 0, 0, 0);
		App13=new AppEntity("com.example.busyservice2", Apptype.LATENCYAPP, 1497600, "MatrixCalculate", 0, 0, 0);
		App14=new AppEntity("com.example.busyservice3", Apptype.LATENCYAPP, 1497600, "MatrixCalculate", 0, 0, 0);
				
//		Queue1.add(App1);
//		Queue1.add(App2);
//		Queue1.add(App3);
//		Queue1.add(App4);
//		Queue1.add(App5);
//		Queue1.add(App6);
//		Queue1.add(App7);
//		Queue1.add(App8);
//		Queue1.add(App9);
		
//		Queue1.add(App10);
		Queue1.add(App11);
//		BusyService.add(App12);
//		BusyService.add(App13);
//		BusyService.add(App14);

//		Queue1.add(App15);
//		Queue1.add(App16);
//		Queue1.add(App17);

//		Queue1.add(App18);
//		Queue1.add(App19);
//		Queue1.add(App20);
//		Queue1.add(App21);
//		Queue1.add(App22);
//		Queue1.add(App23);
//		Queue1.add(App24);
//		Queue1.add(App25);
//		Queue1.add(App26);
//		Queue1.add(App27);
//		Queue1.add(App28);
//		Queue1.add(App29);
//		Queue1.add(App30);
	}
	
	private static void init_Phase(AppEntity app)
	{
		if(app.AppName.equals("pt.isec.tp.am"))
		{
			app.trainingDataPath="freefall_trainingdata";
		app.phases=new Phase[2];
		app.phases[0]=new Phase();
		app.phases[1]=new Phase();
		
		app.phases[1].cpuUsage_max=0.08;
		app.phases[1].cpuUsage_min=0;
		app.phases[1].gpuUsage_max=0.000000000000000000000000001;
		app.phases[1].gpuUsage_min=0;
		app.phases[1].memUsage_max=0.1;
		app.phases[1].memUsage_min=0;
		app.phases[1].centerCPU=0.03754434;
		app.phases[1].centerGPU=0.090275716;
		app.phases[1].centerMEM=0.029726187;
		app.phases[1].freq=1497600;
		app.phases[1].sweetperformance=100;
		app.phases[1].AppType=Apptype.UIAPP;
		app.phases[1].count=0;
		app.phases[1].historyPerform=new double[]{0,0,0,0,0};
		
		app.phases[0].cpuUsage_max=0.25;
		app.phases[0].cpuUsage_min=0.1;
		app.phases[0].gpuUsage_max=0.3;
		app.phases[0].gpuUsage_min=0;
		app.phases[0].memUsage_max=0.1;
		app.phases[0].memUsage_min=0;
		app.phases[0].centerCPU=0.137827316;
		app.phases[0].centerGPU=0.124177423;
		app.phases[0].centerMEM=0.078029726;
		app.phases[0].freq=1036800;
		app.phases[0].sweetperformance=30;
		app.phases[0].AppType=Apptype.GAMEAPP;
		app.phases[0].count=0;
		}
		else if(app.AppName.equals("com.vickie.videojohnoliver"))
		{
			app.trainingDataPath="video_trainingdata";
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			
			app.phases[0].cpuUsage_max=0.3;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0.000000000000000000000000001;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0.1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.198781277;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.033001503;
			app.phases[0].freq=729600;
			app.phases[0].sweetperformance=100;
//			app.phases[0].sweetperformance=200;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=0.3;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0.5;
			app.phases[1].gpuUsage_min=0.000000000000000000000000001;
			app.phases[1].memUsage_max=0.1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.234110919;
			app.phases[1].centerGPU=0.382302033;
			app.phases[1].centerMEM=0.03740184;
			app.phases[1].freq=652800;
			app.phases[1].sweetperformance=24;
//			app.phases[1].sweetperformance=33;
			app.phases[1].AppType=Apptype.VIDEOAPP;
			app.phases[1].count=0;
		}else if(app.AppName.equals("com.replica.replicaisland"))
		{
			app.trainingDataPath="replicaisland_trainingdata";
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			
			app.phases[0].cpuUsage_max=0.005;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0.000000000000000000000000001;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0.1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.015638413;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.034883292;
			app.phases[0].freq=300000;
			app.phases[0].sweetperformance=100;
//			app.phases[0].sweetperformance=5000;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=0.15;
			app.phases[1].cpuUsage_min=0.005;
			app.phases[1].gpuUsage_max=0.8;
			app.phases[1].gpuUsage_min=0.48;
			app.phases[1].memUsage_max=0.1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.06508195;
			app.phases[1].centerGPU=0.52863194;
			app.phases[1].centerMEM=0.041065017;
			app.phases[1].freq=652800;
			app.phases[1].sweetperformance=60;
//			app.phases[1].sweetperformance=50;
			app.phases[1].AppType=Apptype.VIDEOAPP;
			app.phases[1].count=0;
		}else if(app.AppName.equals("com.aemobile.games.luckyfishing")) //training data should be updated before test
		{
			app.trainingDataPath="luckyfishing_trainingdata";
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			
			app.phases[0].cpuUsage_max=1;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=1;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.0327;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.0518;
			app.phases[0].freq=300000;
			app.phases[0].sweetperformance=100;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=1;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=1;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.0801;
			app.phases[1].centerGPU=0.4361;
			app.phases[1].centerMEM=0.0542;
			app.phases[1].freq=300000;
			app.phases[1].sweetperformance=16;
			app.phases[1].AppType=Apptype.VIDEOAPP;
			app.phases[1].count=0;
			app.phases[1].historyPerform=new double[]{0,0,0,0,0};
		}else if(app.AppName.equals("com.vickie.dummyapp"))
		{
			app.trainingDataPath="dummyapp_trainingdata";
			app.phases=new Phase[1];
			app.phases[0]=new Phase();

			app.phases[0].cpuUsage_max=1;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=1;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.0327;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.0518;
			app.phases[0].freq=960000;
			app.phases[0].sweetperformance=16;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
		}else if(app.AppName.equals("com.vickie.myapp1"))
		{
			app.trainingDataPath="myapp1_trainingdata";
			app.phases=new Phase[4];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			app.phases[2]=new Phase();
			app.phases[3]=new Phase();

			app.phases[0].cpuUsage_max=1;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=1;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.0327;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.0518;
			app.phases[0].freq=883200;
//			app.phases[0].sweetperformance=13000;
			app.phases[0].sweetperformance=23000;
			app.phases[0].AppType=Apptype.LATENCYAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=1;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=1;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.0327;
			app.phases[1].centerGPU=0;
			app.phases[1].centerMEM=0.0518;
			app.phases[1].freq=300000;
//			app.phases[1].sweetperformance=16;
			app.phases[1].sweetperformance=100;
			app.phases[1].AppType=Apptype.VIDEOAPP;
			app.phases[1].count=0;
			app.phases[1].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[2].cpuUsage_max=1;
			app.phases[2].cpuUsage_min=0;
			app.phases[2].gpuUsage_max=1;
			app.phases[2].gpuUsage_min=0;
			app.phases[2].memUsage_max=1;
			app.phases[2].memUsage_min=0;
			app.phases[2].centerCPU=0.0327;
			app.phases[2].centerGPU=0;
			app.phases[2].centerMEM=0.0518;
			app.phases[2].freq=1190400;
//			app.phases[2].sweetperformance=18000;
			app.phases[2].sweetperformance=60000;
			app.phases[2].AppType=Apptype.LATENCYAPP;
			app.phases[2].count=0;
			app.phases[2].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[3].cpuUsage_max=1;
			app.phases[3].cpuUsage_min=0;
			app.phases[3].gpuUsage_max=1;
			app.phases[3].gpuUsage_min=0;
			app.phases[3].memUsage_max=1;
			app.phases[3].memUsage_min=0;
			app.phases[3].centerCPU=0.0327;
			app.phases[3].centerGPU=0;
			app.phases[3].centerMEM=0.0518;
			app.phases[3].freq=300000;
			app.phases[3].sweetperformance=20000;
			app.phases[3].AppType=Apptype.LATENCYAPP;
			app.phases[3].count=0;
			app.phases[3].historyPerform=new double[]{0,0,0,0,0};
		}else if(app.AppName.equals("com.qrbarcodescanner"))
		{
			app.trainingDataPath="myapp1_trainingdata";
		app.phases=new Phase[2];
		app.phases[0]=new Phase();
		app.phases[1]=new Phase();
		
		app.phases[0].cpuUsage_max=0.08;
		app.phases[0].cpuUsage_min=0;
		app.phases[0].gpuUsage_max=0.000000000000000000000000001;
		app.phases[0].gpuUsage_min=0;
		app.phases[0].memUsage_max=0.1;
		app.phases[0].memUsage_min=0;
		app.phases[0].centerCPU=0.03754434;
		app.phases[0].centerGPU=0.090275716;
		app.phases[0].centerMEM=0.029726187;
		app.phases[0].freq=1497600;
//		app.phases[0].sweetperformance=100;
		app.phases[0].sweetperformance=400;
		app.phases[0].AppType=Apptype.UIAPP;
		app.phases[0].count=0;
		app.phases[0].historyPerform=new double[]{0,0,0,0,0};
		
		app.phases[1].cpuUsage_max=0.25;
		app.phases[1].cpuUsage_min=0.1;
		app.phases[1].gpuUsage_max=0.3;
		app.phases[1].gpuUsage_min=0;
		app.phases[1].memUsage_max=0.1;
		app.phases[1].memUsage_min=0;
		app.phases[1].centerCPU=0.137827316;
		app.phases[1].centerGPU=0.124177423;
		app.phases[1].centerMEM=0.078029726;
		app.phases[1].freq=1036800;
//		app.phases[1].sweetperformance=33;
		app.phases[1].sweetperformance=100;
		app.phases[1].AppType=Apptype.GAMEAPP;
		app.phases[1].count=0;
		}
		else if(app.AppName.equals("com.davemorrissey.labs.subscaleview.sample"))
		{
			app.trainingDataPath="myapp1_trainingdata";
		app.phases=new Phase[1];
		app.phases[0]=new Phase();
		
		app.phases[0].cpuUsage_max=0.08;
		app.phases[0].cpuUsage_min=0;
		app.phases[0].gpuUsage_max=0.000000000000000000000000001;
		app.phases[0].gpuUsage_min=0;
		app.phases[0].memUsage_max=0.1;
		app.phases[0].memUsage_min=0;
		app.phases[0].centerCPU=0.03754434;
		app.phases[0].centerGPU=0.090275716;
		app.phases[0].centerMEM=0.029726187;
		app.phases[0].freq=1497600;
//		app.phases[0].sweetperformance=100;
		app.phases[0].sweetperformance=400;
		app.phases[0].AppType=Apptype.UIAPP;
		app.phases[0].count=0;
		app.phases[0].historyPerform=new double[]{0,0,0,0,0};
		}
		else if(app.AppName.equals("com.king.candycrushsaga"))
		{
			app.trainingDataPath="com.king.candycrushsaga_trainingdata";
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();

			app.phases[0].cpuUsage_max=0;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0;
			app.phases[0].freq=1574400;
//		app.phases[0].sweetperformance=100;
			app.phases[0].sweetperformance=60;
			app.phases[0].AppType=Apptype.GAMEAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};

			app.phases[1].cpuUsage_max=0;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=0;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerGPU=0;
			app.phases[1].centerMEM=0;
			app.phases[1].freq=1958400;
//		app.phases[1].sweetperformance=33;
			app.phases[1].sweetperformance=400;
			app.phases[1].AppType=Apptype.UIAPP;
			app.phases[1].count=0;
		}else if(app.AppName.equals("com.rovio.angrybirds"))
		{
			app.trainingDataPath="com.rovio.angrybirds_trainingdata";
//			Log.d("AppManagement",app.trainingDataPath);
			app.phases=new Phase[3];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			app.phases[2]=new Phase();

			app.phases[0].cpuUsage_max=0;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0;
//			app.phases[0].freq=1497600;
			app.phases[0].freq=960000;
//		app.phases[0].sweetperformance=100;
			app.phases[0].sweetperformance=60;
			app.phases[0].AppType=Apptype.GAMEAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};

			app.phases[1].cpuUsage_max=0;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=0;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerGPU=0;
			app.phases[1].centerMEM=0;
			app.phases[1].freq=1958400;
//		app.phases[1].sweetperformance=33;
			app.phases[1].sweetperformance=1;
			app.phases[1].AppType=Apptype.UIAPP;
			app.phases[1].count=0;

			app.phases[2].cpuUsage_max=0;
			app.phases[2].cpuUsage_min=0;
			app.phases[2].gpuUsage_max=0;
			app.phases[2].gpuUsage_min=0;
			app.phases[2].memUsage_max=0;
			app.phases[2].memUsage_min=0;
			app.phases[2].centerCPU=0;
			app.phases[2].centerCPU=0;
			app.phases[2].centerGPU=0;
			app.phases[2].centerMEM=0;
			app.phases[2].freq=1958400;
//		app.phases[1].sweetperformance=33;
			app.phases[2].sweetperformance=1;
			app.phases[2].AppType=Apptype.UIAPP;
			app.phases[2].count=0;
		}else if(app.AppName.equals("com.kmplayer"))
		{
			app.trainingDataPath="com.kmplayer_trainingdata";
//			Log.d("AppManagement",app.trainingDataPath);
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();

			app.phases[0].cpuUsage_max=0;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0;
			app.phases[0].freq=300000;
//		app.phases[0].sweetperformance=100;
			app.phases[0].sweetperformance=30;
			app.phases[0].AppType=Apptype.VIDEOAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};

			app.phases[1].cpuUsage_max=0;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=0;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerGPU=0;
			app.phases[1].centerMEM=0;
			app.phases[1].freq=300000;
//		app.phases[1].sweetperformance=33;
			app.phases[1].sweetperformance=100;
			app.phases[1].AppType=Apptype.UIAPP;
			app.phases[1].count=0;
		}else if(app.AppName.equals("com.tatkovlab.sdcardcleaner"))
		{
			app.trainingDataPath="com.tatkovlab.sdcardcleaner_trainingdata";
//			Log.d("AppManagement",app.trainingDataPath);
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();

			app.phases[0].cpuUsage_max=0;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0;
			app.phases[0].freq=652800;
//		app.phases[0].sweetperformance=100;
			app.phases[0].sweetperformance=100;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};

			app.phases[1].cpuUsage_max=0;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=0;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerGPU=0;
			app.phases[1].centerMEM=0;
			app.phases[1].freq=960000;
//		app.phases[1].sweetperformance=33;
			app.phases[1].sweetperformance=100;
			app.phases[1].AppType=Apptype.UIAPP;
			app.phases[1].count=0;
		}else if(app.AppName.equals("com.cmsecurity.lite"))
		{
			app.trainingDataPath="com.cmsecurity.lite_trainingdata";
//			Log.d("AppManagement",app.trainingDataPath);
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();

			app.phases[0].cpuUsage_max=0;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0;
			app.phases[0].freq=422400;
//		app.phases[0].sweetperformance=100;
			app.phases[0].sweetperformance=100;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};

			app.phases[1].cpuUsage_max=0;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=0;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerCPU=0;
			app.phases[1].centerGPU=0;
			app.phases[1].centerMEM=0;
			app.phases[1].freq=729600;
//		app.phases[1].sweetperformance=33;
			app.phases[1].sweetperformance=100;
			app.phases[1].AppType=Apptype.UIAPP;
			app.phases[1].count=0;
		}
	}
}
